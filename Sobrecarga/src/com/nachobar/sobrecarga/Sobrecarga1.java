package com.nachobar.sobrecarga;

public class Sobrecarga1 {
	
	
	public static void main(String[] args) {
		
		Sobrecarga objetoSobrecarga = new Sobrecarga();
		
		System.out.println("Metodo 1");
		objetoSobrecarga.demoSobrecarga();
	
		System.out.println("Metodo 2");
		objetoSobrecarga.demoSobrecarga(3);

	
		System.out.println("Metodo 3");
		int x = objetoSobrecarga.demoSobrecarga(2,3);
		System.out.println(x);
	
		System.out.println("Metodo 4");
		double y = objetoSobrecarga.demoSobrecarga(4.2, 3.4);
		System.out.println(y);

}
}

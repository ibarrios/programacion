package com.nachobar.sobrecarga;

import java.util.Scanner;

public class Operacion {
	static 	Scanner input = new Scanner(System.in);
	
	public static int leerEntero() {
		System.out.println("Introduce un entero");
		int entero = input.nextInt(); 
		return entero;		
	}
	
	
	public static double leerDouble() {
		System.out.println("Introduce un double");
		double decimal = input.nextDouble();
		return decimal;
	}
	
	
	public static int sumar(int x, int y) {		
		int sum = x +y;
		return sum;
		
	}
	
	public static double sumar(double x, double y) {		
		double sum = x + y;
		return sum;
		
	}
	
	public static double sumar (double x, double y, double z) {
		double sum = x + y + z;
		return sum;
	}
	
	public static int sumar (int x, int y, int z, int w) {
		int sum =  x +  y + z + w;
		return sum;
	}
	
	
	public static void main(String[] args) {
		
		int num1 = Operacion.leerEntero();	
		int num2 = Operacion.leerEntero();
		
		
		System.out.println(Operacion.sumar(num1, num2));

		
		input.close();
	}
	
}

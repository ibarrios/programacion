package com.nachobar.sobrecarga;

import java.util.Scanner;

public class Cadenas {
	
	static Scanner input = new Scanner(System.in);
	
	public static String leerCadena() {
		System.out.println("Introduce Cadena");
		String cadena = input.nextLine();
		return cadena;	
	}
	
	public static String sumarCadena(String x, String y) {		
		String cadenasSumadas = x + y;
		return cadenasSumadas;
	}
	
	public static String cadenaMayusculas(String x) {
		String cadenaMayuscula = x.toUpperCase();
		return cadenaMayuscula;
	}
	
	public static void main(String[] args) {
		
		String cadena1 = Cadenas.leerCadena();
		String cadena2 = Cadenas.leerCadena();
		
		System.out.println("Suma de las dos primeras cadenas " + Cadenas.sumarCadena(cadena1,cadena2));
		System.out.println("Primera cadena en mayusculas " + cadenaMayusculas(cadena1));

		
		
		
		input.close();
	}

}

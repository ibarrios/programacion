package com.nachobar.sobrecarga;

public class Sobrecarga {
	
	public  void demoSobrecarga() {	
		System.out.println("Metodo sobrecargado");
	}
	public  void demoSobrecarga(int a) {
		System.out.println("Metodo con un parámetro " + a);						
	}	
	public  int demoSobrecarga(int a, int b) {		
		System.out.println("Metodo on dos parametros " + a + " y " + b);
		return a+b;		
	}
	public  double demoSobrecarga ( double a, double b) {
		System.out.println("Metodo on dos parametros " + a + " y " + b);
		return a+b;
	}	
	

}

package ejercicios07;

public class Metodos07 {
	
	/*Aplicaci�n que llama al m�todo abecedario(). Dicho m�todo no devuelve ni recibe nada y
muestra por pantalla las letras may�sculas de la A-Z de la siguiente forma: AAA, AAB,
AAC,� ABA, ABB, ABC, ABD,� ZZZ. Cada combinaci�n en una l�nea distinta. Desde AAA
hasta ZZZ.*/
	
	public static void abecedario() {
		
		for (int i = 64; i < 90; i++) {
			char primeraLetra = (char)i;
			primeraLetra++;
			for (int j = 64; j < 90; j++) {
				char segundaLetra = (char)j;
				segundaLetra++;
				for (int y = 64; y < 90; y++) {
					char terceraLetra =(char) y;
					terceraLetra++;
					System.out.println(primeraLetra+""+segundaLetra+""+terceraLetra);
					
				}
			}
		}
		
		
	}

	public static void main(String[] args) {
			abecedario();
	}
	
}

package ejercicios01;

import java.util.Scanner;

public class Ejercicio01 {
	
		/*Crea un m�todo en una clase llamada Metodos, que recibe un entero (la edad) y devuelve
	un booleano, verdadero si es mayor de edad o false en caso contrario. Crea una aplicaci�n
	en otra clase (Ejercicio1) que, usando el m�todo de la clase anterior, compruebe si una
	persona es mayor de edad o no. El programa pedir� una edad, y el programa nos mostrar�
	un mensaje dici�ndonos si es mayor o no.*/
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una edad");
		int edad = input.nextInt();
		
		if (Metodos.edad(edad)) {
			System.out.println("Es mayor de edad");
			
		} else {System.out.println("es menor de edad");
		}
		
		
		
		input.close();

	}
}

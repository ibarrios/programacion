package ejercicios03;

import java.util.Scanner;

public class Ejercicio03 {
	
	/*Crea una aplicaci�n (Ejercicio3) que haga uso del m�todo �esDigito�. Este m�todo se crea
en una clase (Metodos) y recibe un tipo char, y devuelve true o false, si el car�cter es una
cifra, o no.*/

		public static void main(String[] args) {
			System.out.println("Introdce un caracter");
			Scanner input = new Scanner(System.in);
			
			
			char caracter = input.nextLine().charAt(0);
			
			if(Metodos.esCifra(caracter)) {
				System.out.println("El caracter es cifra");
			} else {System.out.println("El caracter no es cifra");}
			
			
			input.close();
		}
}

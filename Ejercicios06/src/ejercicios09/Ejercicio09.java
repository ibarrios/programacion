package ejercicios09;

import java.util.Scanner;

public class Ejercicio09 {
	
	/*Aplicaci�n que hace uso del m�todo palabraAleatoria(int longitud). Este m�todo recibe la
longitud de caracteres que queremos que tenga la cadena y nos devuelve un String de esa
longitud con letras aleatorias en el rango A-Z may�sculas. Podemos usar el m�todo del
ejercicio anterior.*/


	public static void palabraAleatoria(int longitud) {
		
		String cadena = "";
		for (int i = 0; i < longitud; i++) {
				cadena += (char)('A'+(Math.random()*25));
		}		
		System.out.println(cadena);
				

	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la longitud ");
		int longitud = input.nextInt();
		
		palabraAleatoria(longitud);
		
		input.close();
	}
}

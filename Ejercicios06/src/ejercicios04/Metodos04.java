package ejercicios04;

public class Metodos04 {
	
	/*Crea una clase (Metodos) que contenga el m�todo esPrimo(). Dicho m�todo recibe un
	n�mero entero e indica si es primo o no (boolean). Crea una aplicaci�n (Ejercicio4) que
	pida al usuario un n�mero entero positivo, y mostrar todos los n�meros primos desde 1
	hasta el n�mero le�do, usando el m�todo creado.*/
	
	public boolean esPrimo(int x) {
		 x = 0;
		int contador = 2;
		boolean primo=true;

		while ((primo) && (contador!= x)){
		  if (x % contador == 0) 
		    return false;
		  contador++;
		  
		}
		return true;
		
	}
	
	

}

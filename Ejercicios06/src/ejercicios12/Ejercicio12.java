package ejercicios12;

import java.util.Scanner;

public class Ejercicio12 {
	
	/*Crear un programa que me muestra una cadena de texto String. Dicha cadena de texto
ser� devuelta por un m�todo leeCaracteres() que no recibe nada. El m�todo ser� el
encargado de leer constantemente caracteres (cifras o letras) hasta que introduzcamos la
cifra 0. La cadena que devuelve dicho m�todo debe ser el resultado de unir todos los
caracteres introducidos por teclado separados por un espacio.*/
	
	
		public static void leeCaracteres() {
			Scanner input = new Scanner(System.in);
			System.out.println("Introduce caracteres");
			char caracter = 0;
			String cadena = "";
			do {
				caracter = input.nextLine().charAt(0);
				cadena += caracter + " ";
			} while (caracter != '0');
			
			System.out.println("El resultado es " + cadena);
			input.close();
		}

	
	
		public static void main(String[] args) {
			
			leeCaracteres();
		}
}

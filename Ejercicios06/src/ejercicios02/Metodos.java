package ejercicios02;

public class Metodos {
	
	/*Crea dos m�todos en una clase (Metodos): El m�todo �maximo� recibe 2 n�meros
enteros, y me devuelve el n�mero mayor. El m�todo �minimo� recibe 2 n�meros y
devuelve el menor. Crea una aplicaci�n (Ejercicio2) que muestre el uso de esos 2 m�todos.*/
	
	public static int maximo(int x, int y) {
			int mayor = 0;
		
		 if ( x > y) {
			 
			 mayor=x;
		 } else { mayor = y;}
		 System.out.println("El mayor es " + mayor);
			return mayor;
	}

	public static int minimo(int x, int y) {
		int menor = 0;
		 
		 if ( x < y) {
			 menor = x;
			
		 } else { menor = y;}
		
		 System.out.println("El menor es " + menor);
		return menor;
	}
}

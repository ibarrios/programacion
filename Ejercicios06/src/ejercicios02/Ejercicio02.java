package ejercicios02;

import java.util.Scanner;

public class Ejercicio02 {
	
	/*Crea dos m�todos en una clase (Metodos): El m�todo �maximo� recibe 2 n�meros
enteros, y me devuelve el n�mero mayor. El m�todo �minimo� recibe 2 n�meros y
devuelve el menor. Crea una aplicaci�n (Ejercicio2) que muestre el uso de esos 2 m�todos.*/
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("introduce dos numeros");
		int enteroA = input.nextInt();
		int enteroB = input.nextInt();
		
		Metodos.maximo(enteroA, enteroB);
		Metodos.minimo(enteroA, enteroA);
		
		
		
		input.close();
	}
}

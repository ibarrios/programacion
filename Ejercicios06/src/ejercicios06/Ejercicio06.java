package ejercicios06;

public class Ejercicio06 {
	
	/*Aplicaci�n que hace uso del m�todo mostrarTablaAscii(). Este m�todo no devuelve nada y
muestra por consola todos los caracteres de la tabla ascii, indicando al lado su c�digo.*/

	public static void mostarTablaAscii() {
		
		for (int i = 0; i < 256; i++) {
			System.out.println("el valor de " + i + " en ascii es " + (char) i);
			
		}
		
	}
	
	
	public static void main(String[] args) {
		mostarTablaAscii();
	}
	
}

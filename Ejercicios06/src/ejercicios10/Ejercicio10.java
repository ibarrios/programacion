package ejercicios10;

import java.util.Scanner;

public class Ejercicio10 {
	
	/*Crear una aplicaci�n que pide una cadena de texto, y mediante el m�todo esEntero(String
cadena), que devuelve un boolean indicando si la cadena corresponde a un n�mero entero
o no. El m�todo esEntero, eval�a todos los caracteres de la cadena (0 - cadena.length()) y
me dice false si encuentra alg�n car�cter que no sea una cifra. Tambi�n debe atender al
signo negativo.*/
	
	
	public static boolean esEntero(String cadena) {

		
		for (int i = 0; i <= cadena.length(); i++) {
			if (cadena.charAt(i) > '0' && cadena.charAt(i) <'9') {
				return true;
			} 		
		}	
		return false;
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("introduce una cadena");
		String cadena = input.nextLine();
		
		if (esEntero(cadena)) {
			System.out.println("Son todos enteros");
		}
		input.close();
	}
}

package ejercicios08;

import java.util.Scanner;

public class Ejercicio08 {
	
	/*Crea una aplicaci�n que nos genere un n�mero aleatorio entero. Para ello haremos uso del
m�todo aleatorio(int inicio, int final), que recibe dos valores enteros, entre los cuales se
crear� el n�mero entero.*/

	public static int enteroAleatorio(int end, int principio) {
		
	
		
		int enteroRandom =(int) Math.round((Math.random()*end)+principio);
		return enteroRandom;
		
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce dos enteros");
		int enteroPrincipio = input.nextInt();
		int enteroFinal = input.nextInt();
		int enteroRandom = enteroAleatorio(enteroFinal, enteroPrincipio);
		
		System.out.println("Este es el numero aleatorio " + enteroRandom);
		
		input.close();
	}
}

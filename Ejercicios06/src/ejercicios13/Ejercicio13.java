package ejercicios13;

import java.util.Scanner;

public class Ejercicio13 {
	
	/*Igual que el ejercicio anterior, pero el m�todo leeCadenas(), lee constantemente cadenas
de caracteres hasta introducir la cadena �fin�. El m�todo devuelve un tipo String, el cual
debe contener todas las cadenas le�das, separadas por el car�cter �:�. Posteriormente el
programa principal mostrar� dicha cadena.*/
	
	public static void leeCadenas() {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce caracteres");
		String cadena = "";
		String cadenaFinal = "";
		do {
			cadena = input.nextLine();
			cadenaFinal += cadena + ":";
			
		} while (cadena.equals("fin") == false);
		
		System.out.println("El resultado es " + cadenaFinal);
		input.close();
	}



	public static void main(String[] args) {
		
		leeCadenas();
	}
}

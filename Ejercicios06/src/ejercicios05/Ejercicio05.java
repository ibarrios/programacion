package ejercicios05;

import java.util.Scanner;

public class Ejercicio05 {

	/*Crea una aplicaci�n (Ejercicio5) que nos genere un n�mero aleatorio entero. Para ello en
una clase nueva (Metodos) crearemos el m�todo aleatorio(int final), que recibe un entero,
y nos genera un n�mero aleatorio entre 0 y final. (necesito usar el m�todo Math.random()
para crear el m�todo aleatorio())*/


	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("introduce un numero ");
		int entero = input.nextInt();
		Metodos05.enteroRandom(entero);
		
		input.close();
	}
}

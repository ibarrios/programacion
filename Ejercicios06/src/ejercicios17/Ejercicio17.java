package ejercicios17;

public class Ejercicio17 {
	
	/*En el paquete �ejercicio17Matematicas�, creamos una clase nueva llamada Matematicas,
que no tiene m�todo main. En ella vamos a crear los siguientes m�todos p�blicos (public):
(solo puedo usar el m�todo random() de la clase Math, para resolverlos)
int absoluto(int a) : Devuelve el valor absoluto de a (el valor absoluto de 2 y de -2 es 2)
double absoluto(double a)
int maximo(int a, int b) : Devuelve el mayor valor entre a o b.
double maximo(double a, double b)
int minimo(int a, int b)
double minimo(double a, double b)
int redondear(double a) : Redondear un n� decimal dependiendo de su primer decimal
int redondearAlza(double a) : Redondea al alza un n� decimal
int redondearBaja(double a) : Redondea a la baja un n� decimal
int potencia(int base, int exponente) : Devuelve la potencia baseexponente
int aleatorio(int final) : Genera n� aleatorio entre 0 y final.
int aleatorio(int inicio, int final) : Genera n� aleatorio entre inicio y final
Creamos otra clase llamada Ejercicio17 dentro de otro paquete llamado �ejercicio17�.
Dentro de su m�todo main y sin crear ninguno m�todo m�s, mostraremos el uso de cada
uno de los m�todos que contiene la clase Matem�ticas.*/

}

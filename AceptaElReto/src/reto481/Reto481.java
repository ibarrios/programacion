package reto481;
import java.util.Scanner;
public class Reto481 {
	public static void main(String[] args) {
		Scanner i = new Scanner(System.in);
		String p = "";
		while (!p.equals("0 0")) {
			p = i.nextLine();
			int f = Integer.parseInt(p.substring(0,p.indexOf(' ')));
			int c = Integer.parseInt(p.substring(p.indexOf(' ')+1));
			switch (f) {
			case 1:
				System.out.println("h" + c);
				break;
			case 2:
				System.out.println("g" + c);
				break;
			case 3:
				System.out.println("f" + c);
				break;
			case 4:
				System.out.println("e" + c);
				break;
			case 5:
				System.out.println("d" + c);
				break;
			case 6:
				System.out.println("c" + c);
				break;
			case 7:
				System.out.println("b" + c);
				break;
			case 8:
				System.out.println("a" + c);
				break;
			}
		}	
		i.close();
	}
}

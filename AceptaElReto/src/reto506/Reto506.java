package reto506;

import java.util.Scanner;

public class Reto506 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		Scanner input = new Scanner(System.in);

		int numeroConsultas = input.nextInt();
		input.nextLine();


		for ( int i = 0 ; i < numeroConsultas ; i++ ) {


			String cadena = input.nextLine();
			int posicionCaracter = cadena.indexOf('/') ;
			int tensionMaxima = Integer.parseInt(cadena.substring(0,posicionCaracter-1));
			int tensionMinima = Integer.parseInt(cadena.substring(posicionCaracter+2));


			if (tensionMaxima >= tensionMinima) {
				System.out.println("BIEN");
			} else System.out.println("MAL");
		}		
		input.close();	
	}
}

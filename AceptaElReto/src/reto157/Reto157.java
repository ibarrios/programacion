package reto157;

import java.util.Scanner;

public class Reto157 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Ram�n se pasa el d�a de Nochevieja contando los minutos que faltan para que den las uvas. �Puedes ayudarle?
		//La entrada consiste en una serie de horas, cada una en una l�nea. 
		//Cada hora est� formada por las horas y los minutos separados por : y 
		//utilizando siempre dos d�gitos. Se utiliza una representaci�n en formato 
		//24 horas (es decir, desde 00:00 a 23:59).
		//La entrada termina cuando la hora es la medianoche (00:00), que no debe procesarse.

		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce varias horas en formato horas minutos XX:XX");
		String horaCompleta1 = input.nextLine();
		String horaCompleta2 = input.nextLine();
		String horaCompleta3 = input.nextLine();
		
		String posicionHora1 =  horaCompleta1.substring(0, 2);
		String posicionHora2 =  horaCompleta2.substring(0, 2);
		String posicionHora3 =  horaCompleta3.substring(0, 2);

		String posicionMinuto1 =  horaCompleta1.substring(3);
		String posicionMinuto2 =  horaCompleta2.substring(3);
		String posicionMinuto3 =  horaCompleta3.substring(3);
	
		int hora1 = Integer.parseInt(posicionHora1);
		int hora2 = Integer.parseInt(posicionHora2);
		int hora3 = Integer.parseInt(posicionHora3);
		
		int minuto1 = Integer.parseInt(posicionMinuto1);
		int minuto2 = Integer.parseInt(posicionMinuto2);
		int minuto3 = Integer.parseInt(posicionMinuto3);
		
		if (hora1 == 00 && hora2 == 00 && hora3 == 00 && minuto1 == 00 && minuto2 == 00 && minuto3 == 00){
			System.out.println("FELIZ A�O NUEVO");
		} else { System.out.println((60*24)-((hora1*60)+minuto1));
				 System.out.println((60*24)-((hora2*60)+minuto2));
				 System.out.println((60*24)-((hora3*60)+minuto3));
		}

		
		input.close();
		
	}

}

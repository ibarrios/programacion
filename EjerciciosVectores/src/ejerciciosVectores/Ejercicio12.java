package ejerciciosVectores;



public class Ejercicio12 {
	/*
	 *Dado un array de n�meros de 5 posiciones con los siguientes valores {1,2,3,4,5}, guardar los valores de este array en otro array distinto, 
	 *pero con los valores invertidos, es decir, que el segundo array deber� tener los valores {5,4,3,2,1}. 
	 * 
	 */
	
	public static void main(String[] args) {

		int arrayPrim[] = {1,2,3,4,5};
		int arraySegun[] = new int [arrayPrim.length];
		int x = arrayPrim.length-1;
		
		for (int i = 0; i < arrayPrim.length; i++) {
			arraySegun[x] = arrayPrim[i];
			x--;
		}
		
		for (int i = 0; i < arraySegun.length; i++) {
			System.out.println("Primer array" + arrayPrim[i]);			
		}
		for (int i = 0; i < arraySegun.length; i++) {
			System.out.println("Segundo array" + arraySegun[i]);			
		}
		
		
	}
}

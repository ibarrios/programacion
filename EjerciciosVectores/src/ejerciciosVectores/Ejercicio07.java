package ejerciciosVectores;


import java.util.Scanner;

public class Ejercicio07 {
	/*
	 * Crea dos arrays de n�meros con una posici�n pasado por teclado.
	 * 
	 * Uno de ellos estar� rellenado con n�meros aleatorios y el otro apuntara al array anterior, despu�s crea un nuevo array con el primer array 
	 * (usa de nuevo new con el primer array) con el mismo tama�o que se ha pasado por teclado, rell�nalo de nuevo con n�meros aleatorios. 
	 * 
	 * Despu�s, crea un m�todo que tenga como par�metros, los dos arrays y devuelva uno nuevo con la multiplicaci�n de la posici�n 0 del array1 
	 * con el del array2 y as� sucesivamente. Por �ltimo, muestra el contenido de cada array.
	 * 
	 */
	
	static int[] multiplica(int [] arraySegundo,int [] arrayPrimero ) {

		int arrayMultiplicado[] = new int [arrayPrimero.length];
		for (int i = 0; i < arrayMultiplicado.length; i++) {
			arrayMultiplicado[i] = arrayPrimero[i]*arraySegundo[i];
			
		}
		
		return arrayMultiplicado;
	}
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la longitud del array");
		int longArray = input.nextInt();
		
		int arrayAleatorio[] = new int[longArray];		
		int arrayPrimero[] = new int [longArray];
		int arraySegundo[] = new int [longArray];
		
		for (int i = 0; i < longArray; i++) {
			arrayAleatorio[i] = (int) (10*Math.random());
			
		}
		
		for (int i = 0; i < longArray; i++) {
			 arrayPrimero[i] = arrayAleatorio[i];

		}
		
		for (int i = 0; i < longArray; i++) {
			 arraySegundo[i] = arrayAleatorio[i] ;

		}
		
		
		int arrayFinal[] = multiplica(arraySegundo, arrayPrimero);
		for (int i = 0; i < arrayFinal.length; i++) {
			System.out.println("Este es la componente para el paramentro " + i +" " + arrayFinal[i]);
			System.out.println(arrayAleatorio[i]);
		}
		
		input.close();
	}
}

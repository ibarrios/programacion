package ejerciciosVectores;

import java.util.Scanner;

public class Ejercicio02 {

	/*
	 * Crea un array de n�meros donde le indicamos por teclado el tama�o del array, 
	 * rellenaremos el array con n�meros aleatorios entre 0 y 9, al final muestra por pantalla 
	 * el valor de cada posici�n y la suma de todos los valores. Haz un m�todo para rellenar 
	 * el array (que tenga como par�metros los n�meros entre los que tenga que generar), 
	 * para mostrar el contenido y la suma del array y un m�todo privado para generar n�mero aleatorio
	 *  (lo puedes usar para otros ejercicios).
	 * 
	 */

	static void mostrarArray(int[] array) {

		for (int i = 0; i < array.length; i++) {
			System.out.println("El valor de la componente " + i + " es " + array[i]);
		}
	}

	static int sumarArray(int[] array) {

		int suma = 0;
		for (int i = 0; i < array.length; i++) {
			suma += array[i];
		}	
		return suma;
	}

	static void rellenarArray(int [] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) (Math.random()*10);			
		}


	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce longitud del array");
		int longArray = input.nextInt();
		int miVector[] = new int [longArray];
		rellenarArray(miVector);
		mostrarArray(miVector);
		System.out.println("La suma de las componentes es " + sumarArray(miVector));


		input.close();
	}
}

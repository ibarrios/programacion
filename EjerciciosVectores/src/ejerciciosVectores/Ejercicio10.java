package ejerciciosVectores;

import java.util.Scanner;

public class Ejercicio10 {
	/*
	 * Crea un array de n�meros y otro de String de 10 posiciones donde insertaremos notas entre
0 y 10 (debemos controlar que inserte una nota valida), pudiendo ser decimal la nota en el array
de n�meros, en el de Strings se insertaran los nombres de los alumnos.
Despu�s, crearemos un array de String donde insertaremos el resultado de la nota con palabras.
 Si la nota esta entre 0 y 4,99, ser� un suspenso
 Si esta entre 5 y 6,99, ser� un bien.
 Si esta entre 7 y 8,99 ser� un notable.
 Si esta entre 9 y 10 ser� un sobresaliente.
Muestra por pantalla, el alumno su
	 * 
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double arrayNotas[] = new double [10];
		String arrayAlumnos[] = new String [10];
		String arrayFinal[] = new String [10];

		
		for (int i = 0; i < arrayAlumnos.length; i++) {
			System.out.println("Introduce la nota");
			arrayNotas[i] = input.nextDouble();	
		}
		
		for (int i = 0; i < arrayAlumnos.length; i++) {
			System.out.println("Introduce al alumno");
			arrayAlumnos[i] = input.next();
		}
		
		for (int i = 0; i < arrayAlumnos.length; i++) {
			if (arrayNotas[i] <= 4.99) {
				arrayFinal[i] = "suspenso";			
			} else if (arrayNotas[i] >= 5 && arrayNotas[i] <= 6.99) {
				arrayFinal[i] = "Bien";
			} else if (arrayNotas[i] >= 7 && arrayNotas[i] <= 8.99) {
				arrayFinal[i] = "Notable";
			} else if (arrayNotas[i] >= 9 && arrayNotas[i] <= 10) {
				arrayFinal[i] = "Sobresaliente";
			}
			
		}
		
		for (int i = 0; i < arrayFinal.length; i++) {
			System.out.println("El alumno " + arrayAlumnos[i] + " ha sacado nota " + arrayNotas[i] + " que es un " + arrayFinal[i]);
		}
		
		
		
		
		input.close();
	}	
}

package ejerciciosVectores;

import java.util.Scanner;



public class Ejercicio08 {
	/*
	 * Crea un array de n�meros de un tama�o pasado por teclado, el array contendr� n�meros aleatorios entre 1 y 300 y mostrar 
	 * aquellos n�meros que acaben en un d�gito que nosotros le indiquemos por teclado (debes controlar que se introduce un numero correcto), 
	 * estos deben guardarse en un nuevo array. 
	 * 
	 * Por ejemplo, en un array de 10 posiciones e indicamos mostrar los n�meros acabados en 5, podr�a salir 155, 25, etc.
	 * 
	 */

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce tama�o del vector");
		int longArray = input.nextInt();
		int contadorArray = 0;
		System.out.println("Introduce numero final");
		int numeroLeido = input.nextInt();


		int array[] = new int [longArray]; 

		if (numeroLeido >=0) {
			for (int i = 0; i < array.length; i++) {
				array[i] = (int) (300*Math.random());

			}		

			for (int i = 0; i < array.length; i++) {

				if ((array[i]+"").endsWith(numeroLeido+"")) {
					contadorArray++;

				} 

			}
			
			int arrayFinal[] = new int [contadorArray];
			int x = 0;
			for (int i = 0; i < array.length; i++) {

				if ((array[i]+"").endsWith(numeroLeido+"")) {
					arrayFinal[x]=array[i];
					x++;

				} 

			}

			for (int i = 0; i < contadorArray; i++) {
				System.out.println("la componente "+ i + " del array final es " + arrayFinal[i] );
			}

		} 

		input.close();	
	}
}

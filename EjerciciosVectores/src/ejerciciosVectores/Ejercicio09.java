package ejerciciosVectores;

import java.util.Scanner;


public class Ejercicio09 {
	/*
	 * Calcula la letra de un DNI, pediremos el DNI por teclado y nos devolver� el DNI completo. 
	 * 
	 * Para calcular la letra, cogeremos el resto de dividir nuestro dni entre 23, el resultado debe estar entre 0 y 22. 
	 * 
	 * Haz un m�todo donde seg�n el resultado de la anterior formula busque en un array de caracteres la posici�n que corresponda a la letra.
	 * 
	 * POSICION LETRA
0 T
1 R
2 W
3 A
4 G
5 M
6 Y
7 F
8 P
9 D
10 X
11 B
12 N
13 J
14 Z
15 S
16 Q
17 V
18 H
19 L
20 C
21 K
22 E
Por ejemplo, si introduzco 70588387, el resultado ser� de 7 que corresponde a �F�.

	 */


	static char letraDNI(int restoNI) {

		char arrayDNI[] = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};

		return arrayDNI[restoNI];
	}
	
	
	public static void main(String[] args) {
		final int DIVISOR  = 23;
		Scanner input = new Scanner(System.in);
		System.out.println("introduce tu DNI sin la letra");
		int numeroDNI = input.nextInt();
			
		int restoDNI = numeroDNI%DIVISOR;
		input.close();
		

		
		System.out.println("La letra de tu DNI " + numeroDNI + " es " + letraDNI(restoDNI));

		
	}

}

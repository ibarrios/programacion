package ejerciciosVectores;

import java.util.Arrays;

public class Ejercicio15 {
	/*
	 * Indica si dos arrays creados por ti son iguales con Equals de Arrays.
	 * 
	 * 
	 */
	public static void main(String[] args) {
		int arrayNuevo[] = new int [10];
		int arrayCopia[] = new int [arrayNuevo.length];
		
		Arrays.fill(arrayNuevo, 2);
		arrayCopia = Arrays.copyOf(arrayNuevo, arrayNuevo.length);
		
		
		if (Arrays.equals(arrayCopia, arrayCopia) ) {
			System.out.println("Son iguales");
			
		} 
		
	
	}
}

package ejerciciosVectores;

import java.util.Scanner;

public class Ejercicio01 {

	/*
	 * Crea un array de 10 posiciones de n�meros con valores pedidos por teclado.
Muestra por consola el �ndice y el valor al que corresponde. Haz dos m�todos, uno para rellenar valores y otro para mostrar.
	 * 
	 */


	static void pedirArray(int [] array) {

		Scanner input = new Scanner(System.in);


		for (int i = 0; i < array.length; i++) {
			System.out.println("Introduce el valor de la componente " +i);
			array[i] = input.nextInt();
		}
		input.close();
	}

	static void mostrarArray(int [] array) {

		for (int i = 0; i < array.length; i++) {
			System.out.println("el valor de la componente " + i + " es " + array[i]);
		}
	}
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la longitud del array");
		int longArray = input.nextInt();

		int elArray [] = new int [longArray];
		pedirArray(elArray);
		mostrarArray(elArray);

		input.close();
	}
}

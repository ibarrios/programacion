package ejerciciosVectores;

import java.util.Scanner;



public class Ejercicio05 {
	/*
	 * Crea un array de caracteres que contenga de la �A� a la �Z� (solo las may�sculas). 
	 * Despu�s, ve pidiendo posiciones del array por teclado y si la posici�n es correcta, 
	 * se a�adir� a una cadena que se mostrara al final, se dejar� de insertar cuando se introduzca un -1.
	 * Por ejemplo, si escribo los siguientes n�meros 
	 * 0 //A�adir� la �A� 
	 * 5 //A�adir� la �F� 
	 * 25 //A�adir� la �Z� 
	 * 50 //Error, inserte otro n�mero 
	 * -1 //fin 
	 * Cadena resultante: AFZ
	 * 
	 */
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		char array[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		int posicion = 0;
		String cadenaFinal = "" ;
		do {
			System.out.println("Introduce posicion del bucle");
			posicion = input.nextInt();
			if (posicion != -1) {
			if (posicion >= 0 && posicion <= 25) {
				cadenaFinal = cadenaFinal + String.valueOf(array[posicion]);
			} else {System.out.println("Error, inserte otro numero");}}
			
		} while (posicion != -1);
		
		System.out.println("La cadena resultante es " + cadenaFinal);
		
		input.close();
	}	
}

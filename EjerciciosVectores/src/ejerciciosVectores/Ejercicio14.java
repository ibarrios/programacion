package ejerciciosVectores;

import java.util.Arrays;

public class Ejercicio14 {
	/*
	 * Crear dos arrays, rellenar uno con n�meros y copiarlo al otro usando CopyOf de Arrays.
	 * 
	 * 
	 * 
	 */
	
	public static void main(String[] args) {
		int arrayNuevo[] = new int [10];
		int arrayCopia[] = new int [arrayNuevo.length];
		
		Arrays.fill(arrayNuevo, 2);
		arrayCopia = Arrays.copyOf(arrayNuevo, arrayNuevo.length);
		
		for (int i = 0; i < arrayCopia.length; i++) {
			System.out.println("Primer array" + arrayNuevo[i] );
		}
		
		for (int i = 0; i < arrayCopia.length; i++) {
			System.out.println("Segundo array" + arrayCopia[i] );
		}
	}
	
}

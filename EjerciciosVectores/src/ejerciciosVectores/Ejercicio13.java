package ejerciciosVectores;

 
import java.util.Arrays;

public class Ejercicio13 {
	/*
	 * Rellenar un array de n�meros (int) usando el m�todo fill de Arrays.
	 * 
	 */
	
	
	public static void main(String[] args) {
		
		int arrayLleno[] = new int [10];
		
		Arrays.fill(arrayLleno, 2);
		
		
		for (int i = 0; i < arrayLleno.length; i++) {
			System.out.println(arrayLleno[i]);
			
		}
	}
}

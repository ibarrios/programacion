package ejerciciosVectores;


import java.util.Scanner;

public class Ejercicio03 {
	/*
	 * Crea un array de n�meros de un tama�o pasado por teclado, el array 
	 * contendr� n�meros aleatorios primos entre los n�meros deseados, por �ltimo, 
	 * nos indica cual es el mayor de todos. Haz un m�todo para comprobar que el 
	 * n�mero aleatorio es primo, puedes hacer todos los m�todos que necesites.
	 * 
	 * 
	 * 
	 */
	

	

	static int [] rellenarArray() {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la longitud del array");
		int longArray = input.nextInt();
		int array[] = new int [longArray];

		for (int i = 0; i < array.length; i++) {
			array[i]= (int) (Math.random()*10);
		}
		input.close();
		return array;
	}
	
	
	static int maxArray(int [] array) {
		int maxArray=array[0];
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] > maxArray)  {
				maxArray = array[i];				
			}
		}
		return maxArray;

	}
	static int primoArray (int [] array ) {
		int contadorPrimo = 0;
		int x = 0;
		int arrayPrimos[] = new int [contadorPrimo];
		
        for(int i = 1; i < array.length; i++)
        {
            if(( array[i] % array[i]) == 0)
            {
                arrayPrimos[x]=array[i];
                contadorPrimo++;
                x++;
            }
        }
        for (int i = 0; i < arrayPrimos.length; i++) {
    		System.out.println("El numero primo de la componente " + i + " es " + arrayPrimos[i]);
		}

		return contadorPrimo;
	}


public static void main(String[] args) {
	
		int miVector [] = rellenarArray();
		
		System.out.println("El numero de primos es " + primoArray(miVector));
		System.out.println("El numeero maximo del array es " + maxArray(miVector));
	
	
}



}

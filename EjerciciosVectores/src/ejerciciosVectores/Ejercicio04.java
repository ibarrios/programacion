package ejerciciosVectores;

public class Ejercicio04 {
	/*
	 * Crea un array de n�meros de 100 posiciones, que contendr� los n�meros del 1 al 100. Obt�n la suma de todos ellos y la media.
	 * 
	 */

	 static void sumaArray(double [] array) {
	
			double sumaArray = 0;
			for (int i = 0; i < array.length; i++) {
				sumaArray += array[i];

			}
			System.out.println("La media del array es " + sumaArray/array.length);
	
	
	}
	 
	 static void rellenarArray(double[]array) {
			for (int i = 0; i < 100; i++) {
				array[i] = i+1;
			}	 	 
	 }
	 
	 static void mostrarArray(double[]array) {
		 for (int i = 0; i < array.length; i++) {
			System.out.println("La componente " + i + " es " + array[i]);
		}
		 
		 
	 }
	  
	public static void main(String[] args) {
	
		double array[] = new double[100];

		rellenarArray(array);
		
		
		
		mostrarArray(array);
		sumaArray(array);

		

		
		
	}





}

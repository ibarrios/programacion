package ejerciciosVectores;

import java.util.Scanner;

public class Ejercicio06 {
	/*
	 * Pide al usuario por teclado una frase y pasa sus caracteres a un array de caracteres. Puedes hacer con o sin m�todos de String.
	 * 
	 */
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena = input.nextLine();
		
		char array[] = cadena.toCharArray();
		
		for (int i = 0; i < array.length; i++) {
			System.out.println("El valor de la componente " + i + " es " + array[i]);
		}
		
	
		
		
		input.close();
	}
	
}

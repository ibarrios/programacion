package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Construir  un  programa  utilizando  las  sentencias  if-else  anidadas,  
		//quepide  el  peso  y  la altura,calcule el �ndice de masa corporal de 
		//una persona (IMC = peso [kg] / altura2[m]) e
		//indique  el estado en el  que  se  encuentra  esa  persona  en  funci�n  del valor  de  IMC: 
			//(*Las magnitudes no son valores enteros)Valor de IMC Diagn�stico
			//< 16 Criterio de ingreso en hospital 
			//de 16 a 17 infrapeso 
			//de 17 a 18 bajo peso 
			//de 18 a 25 peso normal (saludable) 
			//de 25 a 30 sobrepeso (obesidad de grado I)
			//de 30 a 35 sobrepeso cr�nico (obesidad de grado II) 
			//de 35 a 40 obesidad prem�rbida (obesidad de grado III) 
			//>40 obesidadm�rbida (obesidad de grado IV)
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el peso");
		double peso = input.nextDouble();
		System.out.println("Introduce la altura");
		double altura = input.nextDouble();
		
		double imc = peso / Math.pow(altura, 2);
		
		if (imc >= 16 && imc < 17) {
			System.out.println("El paciente tiene infrapeso");
		} else if (imc > 17 && imc < 18) {
			System.out.println("El paciente tiene bajo peso");
		} else if (imc > 18 && imc < 25) {
			System.out.println("El paciente tiene peso normal");
		} else if (imc > 25 && imc < 30) {
			System.out.println("El paciente tiene sobrepeso");
		} else if (imc > 30 && imc < 35) {
			System.out.println("El paciente tiene sobrepeso cronico");
		} else if (imc > 35 && imc < 40) {
			System.out.println("El paciente tiene obesidad premorbido");
		} else {System.out.println("obeso morbido");
		}
		
		input.close();
		
		
	}

}

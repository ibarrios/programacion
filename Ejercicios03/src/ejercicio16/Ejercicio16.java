package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa variaci�n    del    anterior    que    pida una    
		//hora    al    usuario    en    formato horas:minutos:segundosy    
		//posteriormente  pida  una  fecha  en  formato dia/mes/a�o.  
		//El programa  comprueba  si  la  hora  y  fechas  son  v�lidas  
		//y  muestra  un  mensaje  al  usuario indic�ndolo  al  usuario.  
		//Para  separar  los  meses,  d�as,  a�os,  horas,  minutos  y  
		//segundos debemos utilizar los m�todos de la clase String que creamos 
		//oportunos. Debemos atender a que los meses tienen los d�as indicados 
		//en la siguiente tabla:
		//31 diasEnero, Marzo, Mayo, Julio, Agosto, Octubre, Diciembre
		//30 diasAbril, Junio, Septiembre, Noviembre
		//28 diasFebrero (caso normal)
		//29 diasFebrero (Cada 4 a�os, o sea, todos los a�os m�ltiplos de 4)
		
		
Scanner input = new Scanner(System.in);
		

		System.out.println("Introduce dia/mes/a�o");
		String cadena2 = input.nextLine();
		
		

		int caracter1 = cadena2.indexOf('/');
		int caracter2 = cadena2.lastIndexOf('/');
		
		
		String dias = (cadena2.substring(0, caracter1));
		int dia1 = Integer.parseInt(dias);
		
		String meses = (cadena2.substring(caracter1+1, caracter2));
		int mes1 = Integer.parseInt(meses);
		
		String a�os = (cadena2.substring(caracter2+1));
		int a�o1 = Integer.parseInt(a�os);
		
		if (mes1 == 4 || mes1 == 6 || mes1 == 9 || mes1 == 11) {
			if (dia1 >= 1 && dia1 <= 30) {
				if (a�o1 >= 1900) {
					System.out.println("la fecha es correcta");
				} else {System.out.println("La fecha no es correcta");}	 			
			} else {System.out.println("La fecha no es correcta");}
		} else if (mes1 == 1 || mes1 == 3 || mes1 == 5 || mes1 == 7 || mes1 == 8 || mes1 == 10 || mes1 == 12) {
			if (dia1 >= 1 && dia1 <= 31) {
				if (a�o1 >= 1900) {
					System.out.println("la fecha es correcta");
				} else {System.out.println("La fecha no es correcta");}
			} else {System.out.println("La fecha no es correcta");}
		} else if (mes1 == 2) {
			if (dia1 == 28) {
				if (a�o1 >= 1900) {
					System.out.println("la fecha es correcta");
				} else {System.out.println("La fecha no es correcta");}
			} else {System.out.println("La fecha no es correcta");}
		} else if (dia1 == 29) {
			if (mes1 == 2) {
				if (a�o1 % 4 == 0) {
					System.out.println("la fecha es correcta");
				} else {System.out.println("La fecha no es correcta");}
			} else {System.out.println("La fecha no es correcta");}
		} else {System.out.println("La fecha no es correcta");}
			
		input.close();
		
		
	}

}

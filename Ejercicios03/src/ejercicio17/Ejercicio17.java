package ejercicio17;


import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Pedir   dos   fechas   e   indicar   la   cantidad   de   d�as   que   hay   de  
		//diferencia   entre   ellos. Consideramos que todos los meses tienen 30 d�as. 
		//Las fechas se piden como se quiera
		
	
		
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce dia1/mes1");
		String cadena1 = input.nextLine();
		
		int caracter1 = cadena1.indexOf('/');
				
		String dias = (cadena1.substring(0, caracter1));
		int dia1 = Integer.parseInt(dias);
		
		String meses = (cadena1.substring(caracter1+1));
		int mes1 = Integer.parseInt(meses)*30 + dia1;

		
		
		System.out.println("Introduce dia2/mes2");
		String cadena2 = input.nextLine();
		
		int caracter2 = cadena2.indexOf('/');
		
		String dias1 = (cadena2.substring(0, caracter2));
		int dia2 = Integer.parseInt(dias1);
		
		String meses1 = (cadena2.substring(caracter2+1));
		int mes2 = Integer.parseInt(meses1)*30+ dia2;
		
		
		if (mes2 > mes1) {
			System.out.println(mes2 -mes1 + " diferencia de dias");
		} else if (mes1 > mes2 ) {
			System.out.println(mes1 - mes2 + " diferencia de dias");
		} else {System.out.println("La fecha es similar");
		}		
		input.close();		
	}

}

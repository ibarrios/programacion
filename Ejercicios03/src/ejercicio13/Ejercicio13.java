package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa  que  pide  dos  cadenas  de  texto  por  teclado.  
		//El  programa  nos  indicar�  si  son exactamente   iguales,   si   
		//son iguales,aunque   haya   diferencia   entre   may�sculas   y 
		//min�sculas,   o   si   no   son   iguales,   mostrando   un   �nico   
		//mensaje (M�todos   equals   y equalsIgnoreCasede la clase String
		
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce cadena1");
		String cadena1 = input.nextLine();
		System.out.println("introduce cadena2");
		String cadena2 = input.nextLine();
		
	    if (cadena1.equals(cadena2)) {
	    	System.out.println("La cadena es exactamente igual");	    	
	    } else if (cadena1.equalsIgnoreCase(cadena2)) {
	    	System.out.println("La cadena es igual sin tener en cuenta may/min");
	    	} else {System.out.println("las dos cadenas son diferentes");
	    }
	    	
	    	
	    	
		input.close();
	}

}

package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que lea un solo car�cter e indiquesi es unacifra o no lo es (Car�cter entre '0' y '9')(tipo char)
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce caracter");
		char caracter = input.nextLine().charAt(0);
		 
		if (caracter >= 48 && caracter <= 57 ) {
			System.out.println("es una cifra");
		} else {System.out.println("No es una cifra");
		
		}
		
		input.close();
			
		
	}

}

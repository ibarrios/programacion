package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que lea un car�cter por teclado e indiquesi es una letra may�scula(tipo char) 
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un caracter");
		char caracter = input.nextLine().charAt(0);
		
		if (caracter <= 90 && caracter >= 65) {
			System.out.println("caracter es mayuscula");
		} else {System.out.println("caracter no es mayuscula");
		
		}

		input.close();
		
	}

}

package ejercicio26;

import java.util.Scanner;

public class Ejercicio26 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int numero = input.nextInt();


		int sumaDivisores = 0;		
		for(int i = 1; i < numero; i++){
			if(numero % i == 0){
				sumaDivisores = sumaDivisores + i;
			}
		}		
		if(sumaDivisores == numero){
			System.out.println("Es perfecto");
		}else{
			System.out.println("No es perfecto");
		}

		input.close();
	}

}
package ejercicio03;

import java.util.Scanner;

public class Ejercicio03{
	static final String FINAL_PROGRAMA = "El programa ha terminal";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Programa  que  muestre  los  n�meros,  descendiendo  de  uno  en  uno  desde  un  
		//n�mero introducido  por  teclado  mayor  al  1  hasta  el  n�mero  1  utilizando  
		//la instrucci�n  do-while. Realizar el ejercicio mostrando los n�meros cada uno en una l�nea. 
		//Y despu�s mostr�ndolos en  la  misma  l�nea  separados  por  un espacio.  Mostrar  un  mensaje 
		//que  indique  el  final  del programa(constante)
		
		Scanner input = new Scanner(System.in);
		System.out.println("intrduce un numero dos veces");
		
		
		{
			int num = input.nextInt();
			
			do {
			
			System.out.println(--num);
			
		} while (num > 1);}
		
		{ 
			int num = input.nextInt();
			
			do {
			
			System.out.print(--num + " ");
			
		} while (num > 1);}
		
		System.out.println(FINAL_PROGRAMA);
		
		input.close();
		
		
	}

}

package ejercicio30;

import java.util.Scanner;

public class Ejercicio30 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int numero = input.nextInt();
		
		
		boolean esPrimo = true;
		for(int i = 2; i < numero; i++){

			if(numero % i == 0){
				esPrimo = false;
				break;
			}
		}

		if(esPrimo){
			System.out.println("El numero es primo");
		}else{
			System.out.println("El numero no es primo");
		}

		input.close();
	}

}
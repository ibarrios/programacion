package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Pedir un n�mero entero que representa el tama�o del lado. 
		//>Dibujar y rellenar un cuadrado con el car�cter �X�, con tantas filas
		//y columnas de caracteres �X� como se ha indicado por teclado


		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el tama�o del lado como entero");
		int enteroLado = input.nextInt();

		for ( int i = 0 ; i < enteroLado ; i++)
		{
			for ( int y=0; y < enteroLado ; y++)
			{
				System.out.print("X");
			}
			System.out.print("\n");
		}


		input.close();
	}

}

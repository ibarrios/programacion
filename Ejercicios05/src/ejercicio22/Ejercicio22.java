package ejercicio22;

import java.util.Scanner;



public class Ejercicio22 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		Scanner input = new Scanner(System.in);

		double aleatorio = Math.random()*100;
		int random = (int) aleatorio;
		int respuesta = 0;
		System.out.println("Introduce un numero del 1 al 100");
		do {
			respuesta = input.nextInt();
			if (respuesta > 100 || respuesta <= 0 ) {
				System.out.println("Numero incorrecto");
			} else if (respuesta == random ) {
				System.out.println("CORRECTO!");
			}else if (respuesta > random ) {
				System.out.println("Mayor");
			} else { System.out.println("Menor");}
		} while (respuesta != random);


		input.close();
	}

}

package ejercicio31;
import java.util.Scanner;

public class Ejercicio31 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/**Realizar un programa que nos pida un número, y nos diga cuantos números hay entre 1 y n que son primos.**/

		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int limite = input.nextInt();

		int contadorPrimos = 0;
		for(int i = 1; i <= limite; i++){
			int numero = i;
			boolean esPrimo = true;
			for(int j = 2; j < numero; j++){

				if(numero % j == 0){
					esPrimo = false;
					break;
				}
			}
			if(esPrimo){
				contadorPrimos++;
			}
		}
		System.out.println("Hay tantos numeros primos: " + contadorPrimos);
		input.close();
	}
}

package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa  para  controlar  el  nivel  de  un  dep�sito  de  agua.  
		//El  programa  inicialmente  nos pedir� el volumen total en litros del dep�sito. 
		//Posteriormente nos pide constantemente la cantidad de litros que metemos al dep�sito 
		//o que retiramos (n�meros positivos echamos, negativos  retiramos).  Despu�s  de  cada 
		//cantidadque  introducimos,  nos  debe  mostrar  la cantidad restante de litros que hace falta 
		//para llenarlo completamente. El programa deja de pedirnos cantidades cuando se ha llegado o superado 
		//l l�mite del dep�sito, indic�ndonoslo por pantalla. 


		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el volumen total en litros del deposito");
		int volumenTotal = input.nextInt();
		int volumenTanque = 0;
		do {
			System.out.println("Introduce cantidad de litros que metes o sacas del deposito");
			int volumenCambios = input.nextInt();
			volumenTanque = volumenTanque + volumenCambios;
			System.out.println("los litros que quedan sin llenar en el tanque son  " + (volumenTotal - volumenTanque));
		} while (volumenTotal > volumenTanque);
		
		if (volumenTotal == 0) {
		System.out.println("el taque se ha llenado");
		} else  { System.out.println("El tanque ha rebosado");
			
		}
		input.close();

	}
}

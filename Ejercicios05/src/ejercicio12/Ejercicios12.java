package ejercicio12;

import java.util.Scanner;

public class Ejercicios12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//	2)Programa que muestra un men� con 3 opciones. 1 �Opci�n 1, 2 �Opci�n 2, 
		//3 �Salir, y me pide   seleccionar   una  opci�n   a  trav�s   de   un   n�mero  
		//entero.  El  men�   se   mostrar� constantemente  despu�s de  seleccionar  una opci�n,  
		//hasta que  introduzca  la opci�n  Salir, momento en el que termina el programa. Si se 
		//seleccionan las opciones 1 o2 debe mostrar un mensaje indicando la opci�n seleccionada.
		//Si se selecciona salir, mostrar� un mensaje de despedida y terminar� el programa. Usar 
		//switch para obtener la selecci�n.

		Scanner input = new Scanner(System.in);

		int opcion = 0;	

		do {
			System.out.println("1.- Opcion 1\n"
					+ "2.- Opcion 2  \n"
					+ "3.- salir\n"
					+ "Selecciona opcion");

			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				System.out.println("Has seleccionado la opcion 1");
				break;
			case 2:			
				System.out.println("Has seleccionado la opcion 2");
				break;
			case 3:
				System.out.println("Has salido del programa");

				return;

			}

		} while (opcion != 3);
		input.close();

	}
}

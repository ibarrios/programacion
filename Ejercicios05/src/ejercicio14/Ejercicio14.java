package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		//Programa   que   pide   n�meros   enteros   positivos   y   negativos   
		//por   teclado   hasta   que introducimos el n�mero 0. Despu�s deber� indicar 
		//el n�mero total de n�meros le�dos y la suma total de sus valores


		Scanner input = new Scanner(System.in);
		System.out.println("Introduce numeros positivos o negativos");

		int totalLeido = 0;
		int totalSuma = 0;
		int entero;
		do {
			entero = input.nextInt();

			totalLeido++;
			totalSuma = totalSuma + entero;

		} while (entero != 0);

		System.out.println(totalLeido);
		System.out.println(totalSuma);



		input.close();
	}

}

package ejercicio27;

import java.util.Scanner;

public class Ejercicio27 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Crea una aplicaci�n que  dibuje  una escalera de  n�meros, 
		//siendo cada l�nea n�meros empezando en uno y acabando en el n�mero de 
		//la l�nea. Este es un ejemplo, si introducimos un 5 como altura:(2 bucles)

		Scanner input  = new Scanner(System.in);


		int altura = input.nextInt();

		for ( int numero=1 ; numero<=altura ; numero++ ){

			for( int i=1 ; i<=numero ; i++ ){
				System.out.print(i);
			}
			System.out.println("");
			input.close();
		}
	}
}

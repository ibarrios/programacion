package ejercicio24;

import java.util.Scanner;

public class Ejercicio24 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa  que  pida  cadenas  de  texto  hasta  introducir  la  cadena  
		//de  texto  �fin�. Posteriormente me dir� la cantidad de vocales (may�sculas o min�sculas)
		//y el porcentaje de vocales respecto al total de caracteres. Adem�s, me mostrar� 
		//la cadena resultante de unir todas las cadenas separadas por un espacio


		Scanner input = new Scanner(System.in);
		System.out.println("Introduce cadenas");
		String cadena = "";
		int contadorVocales = 0;
		int contadorCaracteres=0;


		do {
			cadena = input.nextLine();
			if (cadena.contentEquals("fin")) {

			} else {
				for (int i = 0 ; i < cadena.length() ; i++ ) {
					if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'e' || cadena.charAt(i) == 'i' || cadena.charAt(i) == 'o' || cadena.charAt(i) == 'u') {
						contadorVocales++;
						contadorCaracteres++;
					} else { 
						contadorCaracteres++;
					}
				}
			}
		} while (!cadena.equals("fin"));
		System.out.println("La cantidad de vocales es " + contadorVocales);
		System.out.println("El porcentaje de caracteres es " + ((contadorVocales*100)/contadorCaracteres));
		input.close();
	}

}

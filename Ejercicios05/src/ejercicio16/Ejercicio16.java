package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Pedir 10 númerosmediante un bucle, y decir al final si se ha introducido alguno negativo
		
		Scanner input = new Scanner(System.in);
		
		int cantidadNegativos = 0;
		System.out.println("Introduce numeros ");
		
		for ( int i = 0 ; i < 10 ; i++ ) {
			
			if (input.nextInt() < 0) {
				
				cantidadNegativos++;			
			}
		}
		System.out.println("la cantidad de numeros negativos introducidos es " + cantidadNegativos);
		
		
		input.close();
		
				
	}

}

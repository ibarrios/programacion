package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Pedir por teclado un número entero ‘cantidadSueldos’. Posteriormente 
		//pedir tantos sueldos como indica la variable ‘cantidadSueldos’. 
		//Finalmente mostrar el sueldo más alto. Utilizar el tipo de datos adecuado

		Scanner input = new Scanner(System.in);


		System.out.println("Introduce la cantidad de sueldos a leer");
		int cantidadSueldos = input.nextInt();
		int resultadoSueldos = 0;
		System.out.println("Introduce sueldos por teclado");
		for (int i = 0 ; i < cantidadSueldos ;i++) {
			 resultadoSueldos = Math.max(resultadoSueldos,input.nextInt());
		} System.out.println(resultadoSueldos);
		input.close();

	}

}

package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa (bucle  for) que  recibe  dos  n�meros  enteros  (un  inicio  y  un  final)  
		//y  despu�s  un tercer n�mero que indica el salto. El programa debe mostrar por pantalla 
		//todos los n�meros desde  el  inicio  hasta  el  final  sumando  o  restando  el  salto
		//(Debe  comprobar  si  el  inicio  es mayor o menor al final, y que el salto sea siempre 
		//un n�mero positivo). Ejemplo1: inicio 1, final 11, salto 2 (1,3,5,7,9,11). 
		//Ejemplo2: inicio 25, final 5, salto 6 (25, 19, 13, 7, 1)
		
			
			
					Scanner input = new Scanner(System.in);

					

					System.out.println("Introduce valor de inicio");
					int inicio = input.nextInt();

					System.out.println("Introduce valor de fin");
					int fin = input.nextInt();

					System.out.println("Introduce el salto");
					int salto = input.nextInt();

					
					if(salto <= 0) {

						System.out.println("El salto debe ser positivo");
					} else {

						if(inicio <= fin) {
						for(int i = inicio; i <= fin; i = i + salto) {

								System.out.print(i + " ");

							}

						} else {
								for(int i = inicio; i >= fin; i-=salto) {

								System.out.print(i + " ");

							}

						}
				}
			
			
			
			input.close();
			
		}
		
		
	}



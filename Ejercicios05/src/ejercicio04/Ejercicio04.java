package ejercicio04;

import java.util.Scanner;



public class Ejercicio04 {
	static final String FINAL_PROGRAMA = "El programa ha terminado";
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que muestre los n�meros desde un n�mero mayor introducido por teclado 
		//hasta otro n�mero menor introducido por teclado, utilizando la instrucci�n for. 
		//Comprobar que el 1er n�mero es mayor que el 2�. Si no es as�, indicarlo mediante
		//un mensaje, y no hacer nada m�s. Realizar  el  ejercicio  mostrando  los  n�meros 
		//cada  uno  en  una  l�nea.  Y  despu�s mostr�ndolos en la misma l�nea separados por 
		//un espacio. Mostrar un mensaje que indique el final del programa(constante).
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un numero mayor");
		int numMayor = input.nextInt();
		
		System.out.println("Introduce un numero menor");
		int numMenor = input.nextInt();
		
		
		if (numMayor < numMenor ) {
			System.out.println("El numero introducido como Mayor no es mayor que el menor");
			
		} for ( int i = numMayor ; i > numMenor ; i-- ) {
			
			System.out.println(i);
			
			
		}
		
		
		System.out.println(FINAL_PROGRAMA);
		input.close();
	}

}

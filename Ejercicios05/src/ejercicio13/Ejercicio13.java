package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		//Programa  que  permite  calcular  la  potencia  de  un  n�mero. Pedir�  
		//por  teclado  la  base  y  el exponente y mostrar� el resultado de la potencia. 
		//El exponente es un enteromayor o igual que 0. La base es un n�mero entero 
		//positivo o negativo. Sin usar la clase Math

		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la base");
		int enteroBase = input.nextInt();
		System.out.println("Introduce el exponente");
		int enteroExponente= input.nextInt();

		int potencia = 0;

		for ( int i = 0 ; i < enteroExponente-1 ; i++ ) {

			potencia = potencia + (enteroBase*enteroBase);
		}

		System.out.println(potencia);
		System.out.println(Math.pow(enteroBase, enteroExponente));




		input.close();
	}



}



package ejercicio25;

import java.util.Scanner;


public class Ejercicio25 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//)Programa que pida n�mero enteros, hasta introducir el n�mero 0. 
		//En ese momento indicar cu�l es el mayor, el menor y la cantidad de n�meros le�da, sin contar el 0

		Scanner input = new Scanner(System.in);
		int entero = 0;
		int enteroMayor = 0;
		int enteroMenor = 0;
		int contador = 0;


		do {
			entero = input.nextInt();

			if (entero != 0 ) {
				if (entero > enteroMayor ) {
					enteroMayor = entero;
				} else if (enteroMenor < entero) {
					enteroMenor = entero + enteroMenor;
				}
				if (entero < enteroMenor ) {
					enteroMenor = entero;
				}
				contador++;
			}
		} while ( entero != 0 );

		System.out.println("El mayor es " + enteroMayor);
		System.out.println("El menor es " + enteroMenor);
		System.out.println("La cantidad de numeros es " + contador);
		input.close();
	}

}

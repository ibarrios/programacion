package com.nachobar.ejemplostatic;

import java.util.Scanner;


public class Circulo {
	static final double NPI = 3.141516;
	static double radio;
	
	 public static double calcularAreaCirculo(double radio) {
		double area =Math.pow(radio, 2)*NPI;
		return area;
	}
	public static double calcularLongitudCirculo(double radio) {
		double longitud = 2*NPI*radio;
		return longitud;		
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el radio");
		radio = input.nextDouble();
		double longitud = calcularLongitudCirculo(radio);
		System.out.println("Longitud: " + longitud);
		double area = calcularAreaCirculo(radio);
		System.out.println("area: " + area );
		input.close();
	}

}

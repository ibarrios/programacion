package com.nachobar.ejemplostatic;

import java.util.Scanner;


public class Lectura {
	static Scanner input = new Scanner(System.in);
	
	
	public static int leerEntero() {
		
		System.out.println("Introduce un entero");
		int entero = input.nextInt();
		
		return entero;
		
	}
	public static double leerDouble() {
		
		System.out.println("Introduce un duouble");
		double decimal = input.nextDouble();
		
		return decimal;
	}
	
	public static String leerCadena() {
		
		System.out.println("Introduce una cadena");
		String cadena = input.nextLine();
		
		return cadena;
		
	}
	
	
	public static void main(String[] args) {
		

		int multiplica = Lectura.leerEntero()*3;
		System.out.println(multiplica);
		double multiplica2 = Lectura.leerDouble()*3;
		System.out.println(multiplica2);
		
	}
}

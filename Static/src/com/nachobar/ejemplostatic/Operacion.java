package com.nachobar.ejemplostatic;

public class Operacion {

	public static double sumar (double x, double y) {
		double suma = x + y;
		return suma;
	}

	public static double restar (double x, double y) {
		double resta = x - y;	
		return resta;
	}
	
	public static double multiplicacion (double x, double y) {
		double multiplicacion = x * y;		
		return multiplicacion;
	}
	
	public static double divide (double x, double y) {
		double division = x / y;
		return division;
	}

	public static void main(String[] args) {
		
		double a = Lectura.leerDouble();
		double b = Lectura.leerDouble();
		
		System.out.println("La suma es: " + Operacion.sumar(a, b));
		System.out.println("La resta es: " + Operacion.restar(a, b));
		System.out.println("La division es: " + Operacion.divide(a, b));
		System.out.println("La multiplicacion es: " + Operacion.multiplicacion(a, b));		
	}	
}

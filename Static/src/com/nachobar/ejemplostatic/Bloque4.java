package com.nachobar.ejemplostatic;

import java.util.Scanner;

public class Bloque4 {
	public static void mostrarValores(int a, int b) {
		
		System.out.println("El valor de a es: " + a);
		System.out.println("EL valor de b es: " + b);
	}
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un numero");
		int x = input.nextInt();
		System.out.println("Introduce oitro numero");
		int y = input.nextInt();
		Bloque4.mostrarValores(x, y);
		
		
		
		
		input.close();

	}
	
}

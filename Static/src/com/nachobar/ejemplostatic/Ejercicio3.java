package com.nachobar.ejemplostatic;

import java.util.Scanner;

public class Ejercicio3 {
	
	static 	Scanner sc=new Scanner(System.in);  
	
	public static int lectorNumeros() {
		
		System.out.println("introduce un numero");
		int entero=sc.nextInt();		
		return entero;		
		
	}
	
	public static void main(String args[]) { 

		int numero1 = Ejercicio3.lectorNumeros();
		int numero2 = Ejercicio3.lectorNumeros();
				
		if (numero1>0){
			System.out.println("El primer numero es positivo");
		}
		else if (numero1<0){
			System.out.println("El primer numero es negativo");
		}
		else if(numero1==0){
			System.out.println("El primer numero es cero");   
		}
		if (numero2>0){
			System.out.println("El segundo numero es positivo");
		}
		else if (numero2<0){
			System.out.println("El segundo numero es negativo");
		}
		else if(numero2==0){
			System.out.println("El segundo numero es cero");   
		}    
		
		
		sc.close();
	}
}


package com.nachobar.ejemplostatic;

import java.util.Scanner;

public class Ejercicio6 {

	static Scanner sc = new Scanner(System.in);  
	
	public static int lector() {
		System.out.println("Introduce un n�mero (negativo para salir):");
		int n = sc.nextInt();
		return n;
	}

	public static int sumar(int x, int y) {
		int suma = x+y;
		return suma;
	}
	
	public static double dividir(double x, double y) {
		double division = x/y;
		return division;
	}
	




	public static void main(String args[]) { 
		int n;
		int minimo,maximo;
		double media;
		int ntotal, suma;

		n = Ejercicio6.lector();

		if (n<0)
			System.out.println("El primer n�mero introducido es negativo");
		else {
			minimo = n;
			maximo = n;
			suma = 0;
			ntotal=0;
			media=0;

			while (n > 0) {
				if (n<minimo) minimo=n;
				if (n>maximo) maximo=n;
				Ejercicio6.sumar(suma, n);
				ntotal++;

				System.out.println("Introduzca un n�mero: ");
				n = Ejercicio6.lector();
			}
			media = Ejercicio6.dividir((double)suma,(double)ntotal);

			System.out.println("minimo " +minimo);
			System.out.println("maximo " +maximo);
			System.out.println("media " +media);


		}    
		sc.close();

	} 

}
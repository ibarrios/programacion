package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		int opcion = 0;

		do {

			System.out.println("1- contar caracteres \n"
					+ "2- contrasenia correcta \n"
					+ "3- Salir \n"
					+ "Introduce una opcion");

			opcion = input.nextInt();
			input.nextLine();

			switch (opcion) {
			case 1:
				System.out.println("Introduce un caracter");				
				char caracter = input.nextLine().charAt(0);
				System.out.println("Introduce una cadena");
				String cadena = input.nextLine();
				int contadorCaracter = 0;
				for (int i = 0 ; i < cadena.length() ; i++ ) {
					if (cadena.charAt(i) == caracter) {
						contadorCaracter++;
					}
				}
				System.out.println("El numero de veces que se repite el caracter es " + contadorCaracter);				
				break;
			case 2:
				System.out.println("Introduce una contrasenia");
				String password = input.nextLine().toLowerCase();
				boolean verificadorVocales = false;
				boolean verificadorLetras = false;
				for ( int i = 0 ; i < password.length(); i++ ) {
					if ( password.charAt(i) == 'a' || password.charAt(i) == 'e' || password.charAt(i) == 'i' || password.charAt(i) == 'o' || password.charAt(i) == 'u' ) {
						verificadorVocales = true;						
					}
					if ( password.charAt(i) >= 'a' && password.charAt(i) <= 'z' ) {
					} else {verificadorLetras = true;}
				}
				if ( verificadorVocales == true && verificadorLetras == false )	{
					System.out.println("La contrasenia es correcta");
				} else {System.out.println("La contrasnia no es valida");}
				break;
			case 3:
				System.out.println("El programa va a terminar");
				break;
			default:
				System.out.println("Has introducido una opcion incorrecta");
				System.err.println();
				break;
			}
		} while (opcion != 3);
		input.close();
	}			
}



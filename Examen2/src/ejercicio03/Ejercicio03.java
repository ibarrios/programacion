package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		String cadena = "";
		int contadorMayusculas = 0;
		int cadenasCincoCaracteres = 0;
		String cadenaLarga = "";
		String cadenaCorta = "";

		System.out.println("Introduce cadenas");
		do {
			cadena = input.nextLine();
			if ( cadena.equals( "fin" ) == true ) {
			} else {
				for ( int i = 0 ; i < cadena.length() ; i++ ) {
					if ( cadena.charAt(i) >= 'A' && cadena.charAt(i) <= 'Z' ) {
						contadorMayusculas++;
					}	
				}
				if ( cadena.length() > 5 ) {
					cadenasCincoCaracteres++;
				}		
				if ( cadena.length() > cadenaLarga.length() ) {
					cadenaLarga = cadena;
				} else if ( cadenaCorta.length() < cadena.length() ) {
					cadenaCorta = cadena;
				}
				if  ( cadena.length() < cadenaCorta.length() ) {
					cadenaCorta = cadena;
				}
			}
		} while ( cadena.equals("fin") != true);
		System.out.println("La cantidad de mayusculas total ha sido de " + contadorMayusculas);
		System.out.println("La cantidad de cadenas de mas de 5 caracteres ha sido de " + cadenasCincoCaracteres);
		System.out.println("La cadena mas larga ha sido " + cadenaLarga);
		System.out.println("La cadena mas corta ha sido " + cadenaCorta);



		input.close();
	}
}

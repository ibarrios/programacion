package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		int entero = 0;
		int sumaPositivos = 0;
		int cantidadPositivos = 0;
		int cantidadNegativos = 0;
		int enterosTotales = 0;
		int enteroMayor = 0;
		int enteroMenor = 0;

		do {
			System.out.println("Introduce enteros");
			entero = input.nextInt();
			if (entero == 0) {
				break;
			}else if (entero > 0 ) {
				cantidadPositivos++;
				enterosTotales++;
				sumaPositivos = entero + sumaPositivos;
			} else {
				cantidadNegativos++;
				enterosTotales++;
			}
			
			if (entero > enteroMayor) {
				enteroMayor = entero;
			} else {
				enteroMenor = entero;
			}

			if (entero < enteroMenor ) {
				enteroMenor=entero;
			}

		} while (entero != 0);

		System.out.println("La suma de los enteros positivos es " + sumaPositivos);
		System.out.println("la media de los positivos es " + sumaPositivos / (cantidadPositivos));
		System.out.println("El porcentaje de negativos respecto al total es " + ((float)cantidadNegativos*100) / ((float)enterosTotales));
		System.out.println("El mayor es " + enteroMayor);
		System.out.println("El menor es " + enteroMenor);

		input.close();

	}

}

package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		/*Ejercicio  4  (2ptos)Programa  que pide  la  fecha  actual  
		 * y  la  fecha  de  nacimiento.  Las  fechas  se  leen en formato �d�a/mes/a�o�.
		 *  Las fechas se introducir�n siempre en ese formato.(No hace falta comprobar que es as�)
		 *  Se leen como un string y se muestran d�a, mes y a�opor separado (1 pto).Posteriormente se
		 *   muestra la edad en a�os de esa persona. La edad no es un valor decimal (1pto).**/

		System.out.println("Introduce la fecha actual en formato dia/mes/anio");
		String fechaActual = input.nextLine();
		System.out.println("Introduce la fecha de nacimiento en formato dia/mes/anio");
		String fechaCumple = input.nextLine();

		int primeraBarraActual = fechaActual.indexOf('/');
		int segundaBarraActual = fechaActual.lastIndexOf('/');

		int primeraBarraCumple = fechaCumple.indexOf('/');
		int segundaBarraCumple = fechaCumple.lastIndexOf('/');

		System.out.println("Dia actual " + fechaActual.substring(0, primeraBarraActual));
		System.out.println("mes actual " + fechaActual.substring(primeraBarraActual+1,segundaBarraActual));
		System.out.println("anio actual " + fechaActual.substring(segundaBarraActual+1)) ;

		System.out.println("Dia cumple " + fechaCumple.substring(0, primeraBarraCumple));
		System.out.println("mes cumple " + fechaCumple.substring(primeraBarraCumple+1,segundaBarraCumple));
		System.out.println("anio cumple " + fechaCumple.substring(segundaBarraCumple+1));

		System.out.println("La edad es " + (Integer.parseInt(fechaActual.substring(segundaBarraActual+1))-Integer.parseInt(fechaCumple.substring(segundaBarraCumple+1))));
		input.close();
	}

}

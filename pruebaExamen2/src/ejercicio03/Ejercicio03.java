package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		for ( int i = 0 ; i < 5 ; i++ ) {
			System.out.println("Introduce cadenas");
			String cadena = input.nextLine();
			boolean contieneVocales = false;
			boolean contieneCifras = false;
			int letrasMayusculas = 0;
			for ( int y= 0 ; y < cadena.length() ; y++) {

				if (cadena.charAt(y) == 'a' || cadena.charAt(y) == 'e' || cadena.charAt(y) == 'i' || cadena.charAt(y) == 'o' || cadena.charAt(y) == 'u' || cadena.charAt(y) == 'A' || cadena.charAt(y) == 'E' || cadena.charAt(y) == 'I' || cadena.charAt(y) == 'O' || cadena.charAt(y) == 'U') {
					contieneVocales = true;					
				}

				if (cadena.charAt(y) >= '0' && cadena.charAt(y) <= '9' ) {
					contieneCifras = true;					
				}

				if (cadena.charAt(y) >= 'A' && cadena.charAt(y) <= 'Z' ) {
					letrasMayusculas++;					
				} 
			} 
			if (contieneVocales == true) {
				System.out.println("Contiene vocales");
			} else {System.out.println("No contiene vocales");		
			}
			if (contieneCifras == true) {
				System.out.println("Contiene cifras");
			} else {System.out.println("No contiene cifras");				
			}
			if (letrasMayusculas == cadena.length()) {
				System.out.println("Toda la cadena es mayusculas");
			} else { System.out.println("No todas las letras son mayusculas");
			}
		}
		input.close();
	}

}

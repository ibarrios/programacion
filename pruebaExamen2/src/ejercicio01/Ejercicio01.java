package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		int opcion;
		int contieneMayuscula = 0;
		do {
                                                                                    
			System.out.println("Introduce una opcion \n"
					+ "1- Factorial\n"
					+ "2- Nombre y apellidos\n"
					+ "3- Salir");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion){
			case 1:
				System.out.println("Introduce un numero entero");
				int entero = input.nextInt();
				int factorial = 1;
				for (int i = entero ; i > 0  ; i-- ) {
					factorial = factorial * i;
				}
				System.out.println("El factorial del entero es " + factorial);
				break;			
			case 2:
				System.out.println("Introduce nombre y apellidos en formato 'Apellidos, Nombre'");
				String cadena = input.nextLine();
				int posicionComa = cadena.indexOf(',');
				System.out.println("Los apellidos son " + cadena.substring(0, posicionComa));
				System.out.println("El nombre es" + cadena.substring(posicionComa+1));
				String nombre = cadena.substring(posicionComa+1);
				for ( int i = 0 ; i < nombre.length() ; i++) {
					if (nombre.charAt(i) >= 'A' && nombre.charAt(i) <= 'Z') {
						contieneMayuscula++;
					}										
				}	
				if (contieneMayuscula > 0) {
					System.out.println("El nombre tiene mayusculas");
					contieneMayuscula = 0;
				} else {System.out.println("El nombre no tiene mayusculas");}
				break;
			case 3:
				System.out.println("Has salido del programa");
				break;
			default:
				System.out.println("Opcion incorrecta");
				break;					
			}
		} while (opcion != 3);

		input.close();
	}

}

package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un email en formato xxxx@xxxxx");
		String email = input.nextLine();

		if (email.contains("@") == false ) {
			System.out.println("el email es incorrecto");
		} else {
			int primerArroba = email.indexOf('@');
			int segundoArroba = email.lastIndexOf('@');
			if (primerArroba != segundoArroba) {
				System.out.println("Se han introducido varias arrobas, el programa va a terminar");
			} else {
				System.out.println("nombre del email " + email.substring(0,email.indexOf('@')));
				System.out.println("dominio del email " + email.substring(email.indexOf('@')+1));
			}		
		}	


		input.close();		
	}
}

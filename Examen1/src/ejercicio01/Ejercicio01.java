package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		System.out.println("1 - Comprobar n�mero\n"
				+ "2 - Comprobar caracter\n"
				+ "3 - Salir\n"
				+ "Elije una opci�n");

		int opcion = input.nextInt();

		switch (opcion) {
		case 1:
			System.out.println("Introduce un entero");
			int entero = input.nextInt();
			if (entero < 0) {
				System.out.println("El numero introducido es negativo");
			} else if (entero % 2 == 0) {
				System.out.println("El numero introducido es par");
			} else {System.out.println("El numero introducido es impar");}		
			break;
		case 2:
			input.nextLine();
			System.out.println("Introduce una cadena");
			String cadena = input.nextLine();
			if (cadena.length() > 1) {
				System.out.println("Has introducido mas de un caracter");				
			} else if (cadena.charAt(0) >= '0' && cadena.charAt(0) <= '9') {
				System.out.println("Has introducido una cifra");				
			} else if (cadena.charAt(0) >= 'A' && cadena.charAt(0) <= 'Z') {
				System.out.println("Es una letra may�scula");
			} else if (cadena.charAt(0) >= 'a' && cadena.charAt(0) <= 'z') {
				System.out.println("Es una letra min�scula");
			}					
			break;
		case 3:
			System.out.println("El programa ha finalizado");
			break;
		default:
			System.out.println("Opcion incorrecta");


		}

		input.close();

	}

}
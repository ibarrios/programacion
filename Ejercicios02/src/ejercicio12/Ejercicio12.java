package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa  que  lee  una  cadena  de  texto  y  
		//posteriormente  un  car�cter,  y  me  dice  si  la cadena
        //comienza por ese car�cter.
		
		
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce texto");
		
		String cadena = input.nextLine();
		
		System.out.println("Introduce caracter");
		String caracter = input.nextLine();
		
		System.out.println(cadena.startsWith(caracter)? "Comienza el caracter de inicio" : "no comienza por el caracter de inicio");
		
		input.close();
	}

}

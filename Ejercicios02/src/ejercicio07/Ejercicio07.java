package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que pide y lee un n�mero de 3 cifras y 
		//muestra cadacifraen una l�nea distinta.Primero  
		//vamos  a  leer  el  numero  como  si  fuerauna  cadena  
		//de  texto  (String,  m�todo nextLine())  y  mostramos 
		//el  resultado.  Despu�s  volvemos  a  pedir  un  n�mero 
		//pero  los leemos  como  un  int  (m�todo  nextInt()).
		//Pista:  para  decomponer  en  cifras String -> m�todos
		//de String, int-> divisi�n y resto de enteros
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("introduce numero");
		String num = input.nextLine();
		System.out.println(num.charAt(0));
		System.out.println(num.charAt(1));
		System.out.println(num.charAt(2));	
		System.out.println("Introduce numero otra vez");
		int num2 = input.nextInt();
		System.out.println(num2 /100);
		System.out.println((num2 / 10)%10);
		System.out.println(num2 % 10);

		input.close();
		
	}

}

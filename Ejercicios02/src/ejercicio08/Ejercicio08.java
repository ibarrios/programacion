package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {
	
		public static void main(String[] args) {
			// TODO Auto-generated method stub	
	
			//Programa que lea un n�mero enterode 5 cifras 
			//y muestre sus cifras desde el principio como 
			//en  el  ejemplo.Utiliza  la  divisi�n  de  enteros
			//y  posteriormente  realiza  lo  mismo pero 
			//trat�ndolo como un String, y aplicando m�todos 
			//de String.
			//1
			//12
			//123
			//1234
			//12345

			Scanner input =  new Scanner (System.in);
						
			System.out.println("Introduce numero entero");
			String texto = input.nextLine();
			System.out.println(texto.charAt(0));
			System.out.println(texto.substring(0, 2));
			System.out.println(texto.substring(0, 3));
			System.out.println(texto.substring(0, 4));
			System.out.println(texto.substring(0, 5));
			
			
			System.out.println("Vuelve a introducir numero entero");
			int num = input.nextInt();

			System.out.println(num / 10000);
			System.out.println(num / 1000);
			System.out.println(num / 100);
			System.out.println(num / 10);
			System.out.println(num);
			
			
		
			input.close();
			
			
}
}

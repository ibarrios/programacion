package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Programa que pide y lee 2 cadenas de texto 
		//por teclado y me dice si son iguales o no 
		//lo son (Operador ?)
		
		 Scanner input = new Scanner (System.in) ; 
		
		 System.out.println("Introduce cadena1");
		 String cadena1 = input.nextLine();
		 //input.nextLine();
		 System.out.println("Introduce cadena2");
		 String cadena2 = input.nextLine();
		 
		 boolean resultado = cadena1.equals(cadena2);
		 
		 System.out.println(resultado == true?"las cadenas son iguales":"las cadenas no son iguales");
		
		input.close();
	}
}

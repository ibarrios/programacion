package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que lea un n�mero enterode 5 cifras 
		//y muestre sus cifras desde el final igual 
		//que en el ejemplo.Utiliza la divisi�n de enteros.
		//5
		//45
		//345
		//2345
		//12345
		
		Scanner input = new Scanner(System.in);
		System.out.println("introduce numero entero");
		int num = input.nextInt();
		System.out.println(num % 10);
		System.out.println(num % 100);
		System.out.println(num % 1000);
		System.out.println(num % 10000);
		System.out.println(num);
		
		
		input.close();
		
	}

}

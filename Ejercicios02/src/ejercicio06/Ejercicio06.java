package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Programa lea la longitud de los catetos de un tri�ngulo 
		//rect�ngulo y calcule la longitud de la hipotenusa seg�n el 
		//teorema de Pit�goras. (Hipotenusa2= cateto12+ cateto22) 
		//Es necesario usar el m�todo Math.sqrt() para calcular la ra�z cuadrada.
		
		Scanner input = new Scanner (System.in);
		
		System.out.println("introduce el cateto1");
		double cateto1 = input.nextDouble();
		System.out.println("introduce el cateto2");
		double cateto2 = input.nextDouble();
		
		double suma = Math.pow(cateto1, 2) + Math.pow(cateto2, 2);
		
		System.out.println(suma);
		
		System.out.println(Math.sqrt(suma));
		
		input.close();
		
		
	}

}

package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Programa  que  lee  una  cadena  por  teclado  y  posteriormente  
		//nos  pregunta  cuantos caracteres del inicio de la cadena queremos 
		///mostrar. Despu�s el programa muestra el n�mero de caracteres indicado 
		//con los que comienza la cadena
		
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("introduce cadena1");
		String cadena1 = input.nextLine();
		System.out.println("introduce numero de caracteres");
		int num = input.nextInt();
		
		System.out.println(cadena1.substring(0,num));
		
		input.close();
		
	}

}

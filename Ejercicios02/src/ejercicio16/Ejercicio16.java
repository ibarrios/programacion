package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Programa que lee una cadena de texto por teclado 
		//y me muestra la segunda mitad de la cadena. Por ejemplo 
		//si una cadena contiene 15 caracteres, me mostrar� los 
		//7  �ltimos.
		
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce cadena de texto");
		String cadena1 = input.nextLine();
		int num = cadena1.length();
		int num2 = num/2;
		
		System.out.println(cadena1.substring(num2));		
		
		   

		input.close();
		
		
	}

}

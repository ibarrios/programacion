package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Programa que lee por teclado el valor del 
		//radio de una circunferencia y calcula y
		//muestra por  pantalla  la  longitud  y el  
		//�rea  de  la  circunferencia.  Longitud 
		//de  la  circunferencia  = 2*PI*Radio, 
		//�rea de la circunferencia = PI*Radio
		
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce el radio de la circunferencia");
		double radio = input.nextDouble();

		 
		System.out.println("Longitud de la circunferencia" +2*Math.PI*radio);
		System.out.println("Area de la circunferencia" + Math.PI * radio);
				
		input.close();
		
	}

}

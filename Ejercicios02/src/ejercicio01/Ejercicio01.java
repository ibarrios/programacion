package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa Java que lee un n�mero entero por teclado 
		//yobtiene y muestra por pantalla el doble y el triple 
		//de ese n�mero
		
		
		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce n�mero");
		int entero = input.nextInt();
		
		System.out.println("este es el doble del entero "  + entero * 2);
		System.out.println("este es el triple del entero "  + entero * 3);
		
		input.close();
		
		
	}

}

package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que pida por teclado la fecha 
		//de nacimiento de una persona (d�a, mes, a�o)
		//y calcule su n�mero de la suerte.La fecha se
		//pide de forma separada, primero d�as, luego 
		//meses, etc. El n�mero de la suerte se calcula 
		//sumando el d�a, mes y a�o de la fecha de nacimiento 
		//y a continuaci�n sumando las cifras obtenidas en la 
		//suma.Si  la  fecha  de  nacimiento  es  12/07/1980,  
		//calculamos  el  n�mero  de  la  suerte  as�: 12+7+1980 
		//= 1999-> 1+9+9+9 = 28

		
		Scanner input = new Scanner(System.in);
		System.out.println("introduce tu d�a de nacimiento");
		int num1 = input.nextInt();
		System.out.println("introduce tu mes de nacimiento");
		int num2 = input.nextInt();
		System.out.println("introduce tu a�o de nacimiento");
		int num3 = input.nextInt();
		
		int num4 = num1 + num2 + num3;
		
		System.out.println(num4);
		System.out.println(num4 / 1000 + (num4 / 100) % 10 + (num4 / 10) % 10 + num4 % 10 );
		
		
		input.close();
		
	}				

}

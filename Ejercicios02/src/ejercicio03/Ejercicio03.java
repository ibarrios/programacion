package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que pida la fecha de nacimiento. 
		//El programa pedir� primero el n�mero, luego 
		//el  mes  escrito  (Enero,  Febrero,  etc.)  
		//y  finalmente  el a�o  en  n�mero.  Debe  
		//mostrar  la fecha  completa  en  una  sola  
		//l�nea.(dia/mes/a�o). Al  leer  el  mes  quiz�s  
		//tengamos  un problema -> Revisar apuntes sobre 
		//vaciar el bufferde lectura, en la clase Scanner.
		
		Scanner input = new Scanner (System.in);
		
		System.out.println("introduce tu dia de nacimiento");
		int dia = input.nextInt();
		
		input.nextLine();
		
		System.out.println("introduce tu mes de nacimiento");
		String mes = input.nextLine();
		
		System.out.println("introduce tu a�o de nacimiento");
		int a�o = input.nextInt();
		
	
		System.out.println((dia + mes + a�o));
		
		
		
		input.close();
	}
}

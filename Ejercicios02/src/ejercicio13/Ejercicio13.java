package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que lee dos cadenas por teclado y me dice 
		//si alguna de las 2 est� contenida en la otra. (Ej: la 
		//cadena �hola papa� contiene la cadena �papa�)
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce cadena1");
		String cadena1 = input.nextLine();
		System.out.println("Introduce cadena2");
		String cadena2 = input.nextLine();
		
		System.out.println(cadena1.contains(cadena2)?"Contiene parte de la cadena":"No contiene parte de la cadena");
				
		input.close();
	}

}

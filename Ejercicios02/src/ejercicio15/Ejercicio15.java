package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que me pide unacadena por teclado, 
		//compuesta de dos palabras separadas por un espacio, 
		//y me muestra por consola la misma cadena, pero mostrando 
		//primero la segunda palabra y luego la primera, separadas 
		//por un espacio.Pista, hay un m�todo que me  indica  la  posici�n  
		//(n�mero)  de  un  car�cter  en  una  cadena  de  texto.  
		//Necesito encontrar el car�cter � � (espacio)
		
		
		Scanner input = new Scanner(System.in);
		System.out.println("introduce dos palabras");
		
		String cadena1 = input.next();
		String cadena2 = input.next();
			
		System.out.println(cadena2 + " " + cadena1);
		
		//String cadena1 = input.nextLine();
		//int  resultado = cadena1.indexOf(' ');
		
		//System.out.println(cadena1.substring(resultado) + " " + cadena1.substring(0, resultado));
		
		
		input.close();
		
	}

}

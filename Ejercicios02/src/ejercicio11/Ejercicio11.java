package ejercicio11;



import java.util.Scanner;

public class Ejercicio11  {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Programa  que  lee  una  cadena  por  teclado  
		//y  me  indica  lo  siguiente:(M�todos  clase String)
		//Si tiene alguna vocal.
		//Si comienza por vocal.
		//Si termina con vocal
				
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce el texto");
		String cadena = input.nextLine();
		String vocala = "a";
		String vocale = "e";
		String vocali = "i";
		String vocalo = "o";
		String vocalu = "u";	
		
	
		System.out.println(cadena.contains(vocala) || cadena.contains(vocale) || cadena.contains(vocali) || cadena.contains(vocalo) || cadena.contains(vocalu)?"tiene vocales":"no tiene ninguna vocal");
		System.out.println(cadena.startsWith(vocala) || cadena.startsWith(vocale) || cadena.startsWith(vocali) || cadena.startsWith(vocalo) || cadena.startsWith(vocalu) ?"empieza por vocal":"no empieza por vocal");
		System.out.println(cadena.endsWith(vocala) || cadena.endsWith(vocale) || cadena.endsWith(vocali) || cadena.endsWith(vocalo) || cadena.endsWith(vocalu) ?"termina por vocal":"no termina por vocal");
		


		input.close();
		
			   }
		}

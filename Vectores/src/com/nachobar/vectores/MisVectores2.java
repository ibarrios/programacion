package com.nachobar.vectores;

import java.util.Scanner;

public class MisVectores2 {
	public static void main(String[] args) {

		String miVector[] = new String[3];

		Scanner input = new Scanner(System.in);

		for (int i = 0; i < 3; i++) {
			System.out.println("Introduce el componente " + i);
			miVector[i]=input.nextLine();
		}

		for (int i = 0; i < 3; i++) {
			System.out.println("La componente " + i + " es " + miVector[i]);
		}

		input.close();
	}

}

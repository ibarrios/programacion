package com.nachobar.vectores;

public class MisVectores9 {
	public static void main(String[] args) {

		int array[] = new int [30];

		int contadorPar = 0;
		int contadorImpar = 0;

		for (int i = 0; i < array.length; i++) {
			array[i] = (int) (Math.random()*99)+1;
		}

		for (int i = 0; i < array.length; i++) {
			if (array[i] % 2 == 0) {
				contadorPar++;
			} else {contadorImpar++;}
		}

		System.out.println("El numero de pares es " + contadorPar);
		System.out.println("El numero de impares es " + contadorImpar);
		for (int i = 0; i < array.length; i++) {
			System.out.println("El valor de la componente " + i + " es " + array[i]);		
		}

	}

}

package com.nachobar.vectores;

public class MisVectores10 {
	public static void main(String[] args) {

		int array[] = new int [30];

		int contadorPar = 0;
		int sumaPares=0;
		int contadorImpar = 0;
		int sumaImpares =0;

		for (int i = 0; i < array.length; i++) {
			array[i] = (int) (Math.random()*99)+1;
		}

		for (int i = 0; i < array.length; i++) {
			if (array[i] % 2 == 0) {
				contadorPar++;
				sumaPares +=array[i]  ;
			} else {contadorImpar++;
			sumaImpares += array[i]  ;
			}
		}

		System.out.println("El numero de pares es " + contadorPar);
		System.out.println("El numero de impares es " + contadorImpar);
		System.out.println("La suma de los pares es " + sumaPares);
		System.out.println("La suma de los impartes es " + sumaImpares);

		for (int i = 0; i < array.length; i++) {
			System.out.println("El valor de la componente " + i + " es " + array[i]);		
		}

	}

}
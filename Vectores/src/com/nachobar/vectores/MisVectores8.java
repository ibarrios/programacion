package com.nachobar.vectores;

import java.util.Scanner;

public class MisVectores8 {
	public static Scanner input = new Scanner(System.in);


	public static int[] insertarDatosArray() {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce longitud del vector" );
		int longArray = input.nextInt();
		int [] array = new int[longArray];
		for (int i = 0; i < array.length; i++) {
			System.out.println("Introduce el parametro " + i);
			array[i] = input.nextInt();
			input.close();
		}	
		return array;

	}


	public static int sumarArray(int[] array) {
		int suma = 0;
		for (int i = 0; i < array.length; i++) {
			suma += array[i];		
		}	
		return suma;
	}

	public static int maxArray(int[] array) {

		int maximo = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > maximo) {
				maximo=array[i];
			}
		}	
		return maximo;
	}


	public static int promedioArray(int[] array) {
		int suma = 0;
		for (int i = 0; i < array.length; i++) {
			suma += array[i];
		}
		int promedio = suma/array.length;

		return promedio;
	}


	public static void main(String[] args) {

		int miVector[]=insertarDatosArray();
		System.out.println("La suma de las componentes es " + sumarArray(miVector));
		System.out.println("El maximo de las componentes es " + maxArray(miVector));
		System.out.println("El promedio de las componentes es " + promedioArray(miVector));

	}
}

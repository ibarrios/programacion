package com.nachobar.vectores;

import java.util.Scanner;

public class MisVectores6 {

	public static void main(String[] args) {


		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce la longitud del vector");
		int longVect = input.nextInt();
		int miVector[] = new int[longVect];
		
		for (int i = 0; i < longVect; i++) {
			System.out.println("introduce los parametros de la componente " + i);
			miVector[i] = input.nextInt();			
		}
		int opcion = 0;
		do {
			System.out.println("Introduce una opcion\n"
					+ "1.- suma\n"
					+ "2.- muestra la media\n"
					+ "3.- calcula el minimo\n"
					+ "4.- calcula el maximo\n"
					+ "5.- Salir");
			opcion = input.nextInt();
			switch (opcion) {
			case 1:
				int suma=0;
				for (int i = 0; i < miVector.length; i++) {
					suma += miVector[i];
				}
				System.out.println("La suma de las componentes es " + suma);
				break;
			case 2:
				int sumaMedia=0;
				for (int i = 0; i < miVector.length; i++) {
					sumaMedia += miVector[i];
				}
				System.out.println("la media es " + sumaMedia/miVector.length);
				break;
			case 3:
				int minimo =miVector[0];
				for (int i = 0; i < miVector.length; i++) {
					if (miVector[i] < minimo) {
						minimo=miVector[i];
					}
				}	
				System.out.println("el minimo es "+minimo);
				break;
			case 4:
				int maximo=miVector[0];
				for (int i = 0; i < miVector.length; i++) {
					if (miVector[i] > maximo) {
						maximo = miVector[i];
					}
				}
				System.out.println("El maximo es: " + maximo);
				break;
			case 5:
				System.out.println("Has salido del programa");
				System.exit(0);
				break;
			default:
				System.out.println("Opcion incorrecta");
				break;
			}
		} while ( opcion != 5);
		input.close();
	}
}

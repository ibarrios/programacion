package ejercicio04;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Programa que declare,al menos, cuatro variablesde tipo intvariableA, 
		//variableB, variableC y variableD,as�gnale un valor acada unay muestra su 
		//valor por consola. A continuaci�n,realiza lasasignacionesnecesarias para que: 
			//Atome el valor de B, B tome el valor de C, Ctome el valor de D, D tome el valor de A.
			//Muestra de nuevo su valor para comprobarlo.�Qu� m�s necesitamos si no queremos 
			//perder ning�n valor
		
		int variableA = 1;
		int variableB = 2;
		int variableC = 3;
		int variableD = 4;
		
		System.out.println(variableA);
		System.out.println(variableB);
		System.out.println(variableC);
		System.out.println(variableD);
		
		 int aux = variableA;
		
		 variableA = variableB;
		 variableB = variableC;
		 variableC = variableD;
		 variableD = variableA;
		 		
		 
		System.out.println(variableA);
		System.out.println(variableB);
		System.out.println(variableC);
		System.out.println(variableD);
		System.out.println(aux);
		
	
	}

}

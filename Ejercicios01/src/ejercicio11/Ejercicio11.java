package ejercicio11;

public class Ejercicio11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Crea 2 variables de tipo short, y 2 de tipo long. 
		//Dales a las variables de tipo long el m�ximo mayor  
		//positivo,  y  el  m�nimo  negativo.  Luego  asigna  
		//cada  una  de  esas  variables  long  a  las variables 
		//de tipo short. Utiliza la conversi�n expl�cita (cast)y 
		//muestra las 4 variables por pantalla. Adem�s muestra un 
		//mensaje indicando qu� es lo que ocurre en ambos casos
		
		
		long maximoL= 9223372036854775807L;
		long minimoL = -9223372036854775808L;
		
		short vara = (short) maximoL;
		short varb = (short) minimoL;
		
		System.out.println(vara);
		System.out.println(varb);
		System.out.println(maximoL);
		System.out.println(minimoL);
		System.out.println(Long.MAX_VALUE);
		
		short numero = 511;
		byte letra = (byte) numero;
		System.out.println(letra);

		
	}

}

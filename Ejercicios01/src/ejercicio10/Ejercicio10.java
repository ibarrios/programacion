package ejercicio10;

public class Ejercicio10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa que  declare  una  variable variableA de  tipo  entero  y  as�gnale  un  valor.  
		//A continuaci�n,muestra  un 3  mensajes:  uno indicando  si  el  valor  de  Aes  m�ltiplo  
		//de  5, otro indicando si es m�ltiplo de 10 y 
		//despu�sotro  indicandosi  es  mayor,menor,  o  igual  a100(3  posibilidades,  1  solo 
		//mensaje). Consideraremos el 0 como positivo. Si no, que memuestre otro mensaje 
		//>(p.e. �No se cumplenlas condiciones�)
		
		
		int num = 100;
		
		System.out.println(num % 5 == 0? "Multiplo de 5" : "No es multiplo de 5" );
		System.out.println(num % 10 == 0? "Multiplo de 10" : "No es multiplo de 10" );
		System.out.println(num >= 100 ? (num == 100 ? "es igual a 100" : "es mayor que 100") : "es menor que 100");

}
}
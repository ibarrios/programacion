package ejercicio08;

public class Ejercicio8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programaque  declare  una  variableA  de tipo  entero  y  
		//as�gnale  un  valor.  A  continuaci�n muestra un mensaje 
		//indicando si A es par o impar. Utiliza el operador 
		//condicional ( ? : ) dentro del println para resolverlo. 
		//En otro mensaje indicar siAes ala vez m�ltiplo de 2 (par) 
		//y m�ltiplo de 3
		
		
		int num = 10;
		
		System.out.println(num % 2 == 0? "num es par" : "num es impar");
		System.out.println((num % 2 == 0) && (num % 3 == 0)? "num es par y multiplo de 3 a la vez" : "num no es par y multiplo de 3 a la vez");

		
	}

}

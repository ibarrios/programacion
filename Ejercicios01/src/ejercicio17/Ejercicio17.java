package ejercicio17;


public class Ejercicio17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Mediante  el  operador  ?  y  creando  dos  variables  
		//tipo  String,  debemos  inicializarlas  con  2 
		//cadenas de texto. El programa debe comparar ambas 
		//cadenas y mostrarme uno de los siguientes mensajes, 
		//dependiendo del valor de las cadenas:
		//�-�cadena1� es mayor que �cadena2�
		//�-�cadena2� es mayor que  �cadena1�
		//�-�cadena1� es igual que �cadena2�			
		//Donde �cadena1� y �cadena2�	son los valores que he almacenado
		//en ambas variables.Las cadenas  se  comparan  atendiendo  
		//al  valor  de  sus  caracteres.  Usar alg�n m�todo  de  
		//la  clase String.
		
	
		String cadena1 = "O";
		String cadena2 = "O";
		int resultado = cadena1.compareTo(cadena2);
		
		
		System.out.println(resultado);
		System.out.println(resultado == 0 ?"cadena1 es igual que cadena2" : (resultado >= 1? "Cadena1 es mayor que cadena2":"cadena2 es mayor que cadena1") );
		
	
		
		
	}

}

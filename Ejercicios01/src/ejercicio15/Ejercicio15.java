package ejercicio15;


public class Ejercicio15 {
	static final String MENSAJE_INICIO = "Comienza el programa";
	static final String MENSAJE_FIN = "Termina el programa";
	
	//convenci�n de las constantes, siempre en may�sculas con barra baja
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Crea  una  variable  de  tipo  String  que  contenga  
		//una  cadena  de  texto  larga  (m�s  de  20 caracteres). 
		//Utiliza el m�todo substring de la clase String, para obtener 
		//una cadena intermedia, dentro  de  la  cadena  principal.  
		//Adem�s  se  deben  crear  2  constantes tipo  String.  
		//Una  con  el mensaje �Comienza el programa� y la otra con 
		//�Termina el programa�. Mostrar cada una antes y 
		//despu�s del resto del c�digo
		

		String cadena = "este es el ejercicio 15 de la clase String";
	
		
		System.out.println(cadena.length());
	
		System.out.println(MENSAJE_FIN+cadena.substring(10, 21)+MENSAJE_FIN);
		System.out.println(cadena.substring(12).length());
		
		
		
	}

}

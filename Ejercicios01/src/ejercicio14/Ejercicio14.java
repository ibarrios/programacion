package ejercicio14;

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Crea 4variables String y les introducimos una cadena que represente 
		//un n�merodecimal, un n�mero entero largo (long), un n�mero tipo byte,
		//y un n�mero int. Ahora crea 4 variables de los  tipos  anteriores,  y  
		//asigna  el  valor  de  las  variables  String  a  las  variables  del  
		//tipo  concretousando los m�todos necesariospara convertir los datos
		
	
		
	    String decimal = "3.3";
		String largo = "3444";
		String entero = "1";
     	String corto = "10";
     	
		System.out.println(Double.parseDouble(decimal));
		System.out.println(Long.parseLong(largo));
		System.out.println(Integer.parseInt(entero));
		System.out.println(Byte.parseByte(corto));
		
		System.out.println(String.valueOf(decimal));
		System.out.println(String.valueOf(largo));
		System.out.println();
		System.out.println();
	}

}

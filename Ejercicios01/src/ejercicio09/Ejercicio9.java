package ejercicio09;

public class Ejercicio9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Utiliza el operador condicional ( ? : ) dentro del println paraque 
		//me indique si el valor de una variable int (le asignamos un valor)es o 
		//m�ltiplo de 3 om�ltiplo de 7, y adem�s tiene que ser positivo.  
		//En caso afirmativo nos dir� que cumple las caracter�sticas nombradas, 
		//y si no, que no las cumple
		
		int num = -6;
		
		System.out.println((num % 3 == 0 || num % 7 == 0) && num >=0? "cumple" : "no cumple");
	}

}

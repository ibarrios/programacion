package ejercicio05;

public class Ejercicio5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int a = 5;
		int b = 3;
		int c = -12;
		
		boolean resultadoA = a > 3;
		boolean resultadoB = a > c;
		boolean resultadoC = a < c;
		boolean resultadoD = b < c;
		boolean resultadoE = b != c;
		boolean resultadoF = a == 3;
		boolean resultadoG = a * b == 15;
		boolean resultadoH = a * b == 30;
		boolean resultadoI = c / b < a;
		boolean resultadoJ = c / b == 10;
		boolean resultadoK = c / b == 4;
		boolean resultadoL = a + b + c == 5;
		boolean resultadoM = ( a + b == 8) && (a - b == 2);
		boolean resultadoN = (a + b == 8 || (a - b == 6));
		boolean resultadoO = a > 3 && b > 3 && c < 3;
		boolean resultadoP = a > 3 && b > - 3 && c < - c;
		
		
		System.out.println(resultadoA);
		System.out.println(resultadoB);
		System.out.println(resultadoC);
		System.out.println(resultadoD);
		System.out.println(resultadoE);
		System.out.println(resultadoF);
		System.out.println(resultadoG);
		System.out.println(resultadoH);
		System.out.println(resultadoI);
		System.out.println(resultadoJ);
		System.out.println(resultadoK);
		System.out.println(resultadoL);
		System.out.println(resultadoM);
		System.out.println(resultadoN);
		System.out.println(resultadoO);
		System.out.println(resultadoP);
		
	}

}

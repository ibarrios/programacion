package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Pedir  una  nota  num�rica  entera  entre  0  y  10  y  mostrar  el  valor  de  la  
		//nota:  Insuficiente, Suficiente,  Bien,  Notable  o  Sobresaliente.  
		//(No  es  necesario  poner  break;  en  todos  los casos.
		
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un entero entre  0 y 10");
		int entero = input.nextInt();
		
		switch (entero) {
		case 0:
			System.out.println("insuficiente");
			break;
		case 1:
			System.out.println("insuficiente");
			break;
		case 2:
			System.out.println("insuficiente");
			break;	
		case 3:
			System.out.println("insuficiente");
			break;	
		case 4:
			System.out.println("insuficiente");
			break;
		case 5:
			System.out.println("Suficiente");
			break;
		case 6:
			System.out.println("bien");
			break;
		case 7:
			System.out.println("notable");
			break;
		case 8:
			System.out.println("notable");
			break;
		case 9:
			System.out.println("sobresaliente");
			break;
		case 10:
			System.out.println("sobresaliente");
			break;
		default:
			System.out.println("");
			
		}
		
		input.close();
	}

}

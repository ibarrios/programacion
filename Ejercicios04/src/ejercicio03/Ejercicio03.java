package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Pedir una nota num�rica entera entre 0 y 10,y mostrar dicha nota de la forma: 
		//cero, uno, dos, tres... Comprobar si la opci�n introducida no es v�lida.
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un entero entre  0 y 10");
		int entero = input.nextInt();
		
		switch (entero) {
		case 0:
			System.out.println("El numero introducido es el cero");
			break;
		case 1:
			System.out.println("El numero introducido es el uno");
			break;
		case 2:
			System.out.println("El numero introducido es el dos");
			break;
		case 3:
			System.out.println("El numero introducido es el tres");
			break;
		case 4:
			System.out.println("El numero introducido es el cuatro");
			break;
		case 5:
			System.out.println("El numero introducido es el cinco");
			break;
		case 6:
			System.out.println("El numero introducido es el seis");
			break;
		case 7:
			System.out.println("El numero introducido es el siete");
			break;
		case 8:
			System.out.println("El numero introducido es el ocho");
			break;
		case 9:
			System.out.println("El numero introducido es el nueve");
			break;
		case 10:
			System.out.println("El numero introducido es el diez");
			break;
		default:
			System.out.println("ERROR");
		}
				
		
		
		
		input.close();
	}

}

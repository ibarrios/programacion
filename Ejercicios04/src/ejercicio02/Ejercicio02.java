package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa  que pida  al  usuario  una  cantidad  de  gramos y  posteriormente 
		//pida  seleccionar una  opci�n  de  un  men�  con  opciones  num�ricas (opciones  
		//del men�:1-Kilogramo, 2-Hectogramos, 3-Decagramos, 4-Decigramos, 5-Centigramos, 6-Miligramos). 
		//Al  seleccionar una  opci�n  mostrar  la  conversi�n de  gramos a  dicha  unidad. Comprobar  si  la  
		//opci�n del men� introducida  no  es  v�lida. Si  la  cantidad  de  gramos  es  un  valor  negativo,  no  hacer nada
		
		Scanner input = new Scanner(System.in);
		System.out.println("introduce una cantidad en gramos");
		int gramos = input.nextInt();
		
		System.out.println("Elige una opcion \n"
				+ "1- Kilogramos\n"
				+ "2- Hectogramos\n"
				+ "3- Decagramos\n"
				+ "4- Decigramos\n"
				+ "5- Centigramos\n"
				+ "6- Miligramos");
		int opc = input.nextInt();
		
		switch (opc) {
		case 1:
			if(gramos < 0) {
				System.out.println();
			} else {System.out.println(gramos * 0.001 + " Kilogramos");}
			break;
		case 2:
			if(gramos < 0) {
				System.out.println();
			} else {System.out.println(gramos * 0.01 + " Hectogramos");}
			break;
		case 3:
			if(gramos < 0) {
				System.out.println();
			} else {System.out.println(gramos * 0.1 + " Decagramos");}
			break;
		case 4:
			if(gramos < 0) {
				System.out.println();
			} else {System.out.println(gramos * 10 + " Decigramos");}
			break;
		case 5:
			if(gramos < 0) {
				System.out.println();
			} else {System.out.println(gramos * 100 + " Centrigramos");}
			break;
		case 6:
			if(gramos < 0) {
				System.out.println();
			} else {System.out.println(gramos * 1000 + " Miligramos");}
			break;
		default:
			System.out.println("Opcion incorrecta");
			
		}

		
		input.close();
		
	}

}

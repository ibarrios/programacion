package ejercicio01;

import java.util.Scanner;

public class Ejercicio01a {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//1a.  Programa  Java  que  lea  un car�cter  en  min�scula(no  hace  falta  comprobarlo)
		//y, utilizando una sentencia switch,diga qu� vocal es, o en caso contrario, que indique que no 
		//es  vocal,  y muestre el  car�cter  en  ambos  casos.  Probar  el  ejercicio  usando  la  
		//sentencia break  en  los  cases.1b Mismo ejercicio, pero sin usar sentencia break en los �cases�.
		//Mostrar un mensaje al final del programa explicando qu� ocurre.
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un caracter");
		char letra = (char) input.nextLine().charAt(0);
		
		switch (letra) {
		case 'a':
			System.out.println("es la vocal " + letra);
			break;
		case 'e':
			System.out.println("es la vocal " + letra);
			break;
		case 'i':
			System.out.println("es la vocal " + letra);
			break;
		case 'o':
			System.out.println("es la vocal " + letra);
			break;
		case 'u':
			System.out.println("es la vocal " + letra);
			break;
		default:
			System.out.println("No es una vocal, es el caracter " + letra);
			break;
		
		}
		
		
		
		input.close();
		
		
	}

}

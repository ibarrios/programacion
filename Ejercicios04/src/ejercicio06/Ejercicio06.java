package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Programa  que  pida  2  n�meros float y  a  continuaci�n  un  car�cter  
		//de  operaci�n aritm�tica (+,-,*,/,%). Mediante  una  sentencia  switch  
		//escoger  el  caso  referente  al car�cterde operaci�n introducido. 
		//El programa mostrar� un mensaje con el resultado de la operaci�n escogida, 
		//entre los 2 n�meros. Indicarsi el car�cter introducido no es v�lido
		
		Scanner input = new Scanner (System.in);
		System.out.println("Introduce decimal1");
		float decimal1 = input.nextFloat();
		System.out.println("Introduce decimal2");
		float decimal2 = input.nextFloat();
		input.nextLine();
		System.out.println("Introduce un operador aritm�tico");
		String opc = input.nextLine();
		
		switch (opc) {
		case "+":
			System.out.println("el resultado es " + (decimal1 + decimal2));
			break;
		case "-":
			System.out.println("El resultado es " + (decimal1 - decimal2));
			break;
		case "*":
			System.out.println("El resultado es " + decimal1 * decimal2);
			break;
		case "/":
			System.out.println("El resultado es " + decimal1 / decimal2);
			break;
		case "%":
			System.out.println("El resultado es " + decimal1 % decimal2);
			break;
		default:
			System.out.println("Caracter incorrecto");
		}
		
		System.out.println();
		
		input.close();
		
	}

}

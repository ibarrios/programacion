package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		
		
	
		int resultadoPositivos = 0;
		int resultadoMultiplos = 0;
		
		System.out.println("introduce un numero");
		int primerEntero = input.nextInt();
		if (primerEntero > 0) {
			resultadoPositivos = resultadoPositivos+primerEntero;
		}   			
		if (primerEntero % 2 == 0 || primerEntero % 3 == 0 ) {
			resultadoMultiplos ++;
		}
		
		System.out.println("introduce un numero");
		int segundoEntero = input.nextInt();
		if (segundoEntero > 0) {
			resultadoPositivos = resultadoPositivos+segundoEntero;
		}
		if (segundoEntero % 2 == 0 || segundoEntero % 3 == 0 ) {
			resultadoMultiplos ++;
		}
		
		System.out.println("introduce un numero");
		int tercerEntero = input.nextInt();
		if ( tercerEntero > 0) {
			resultadoPositivos = resultadoPositivos+tercerEntero;
		}
		if (tercerEntero % 2 == 0 || tercerEntero % 3 == 0 ) {
			resultadoMultiplos ++;
		}
		
		System.out.println("introduce un numero");
		int cuartoEntero = input.nextInt();
		if ( cuartoEntero > 0) {
			resultadoPositivos = resultadoPositivos+cuartoEntero;
		}
		if (cuartoEntero % 2 == 0 || cuartoEntero % 3 == 0 ) {
			resultadoMultiplos ++;
		}
		
		System.out.println("introduce un numero");
		int quintoEntero = input.nextInt();
		if (quintoEntero > 0) {
			resultadoPositivos = resultadoPositivos+quintoEntero;
		}
		if (quintoEntero % 2 == 0 || quintoEntero % 3 == 0 ) {
			resultadoMultiplos ++;
		}

		
		System.out.println("Suma de los positivos " + resultadoPositivos);
		System.out.println("Suma de los multiplos " + resultadoMultiplos);

		input.close();
	}

}

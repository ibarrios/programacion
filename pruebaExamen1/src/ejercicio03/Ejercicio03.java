package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner (System.in);
		
		System.out.println("Introduce opcion: \n"
				+ "1.- Comprobar cadena \n"
				+ "2.- Separar cifras \n"
				+ "3.- Salir");
		int opcion = input.nextInt();
		
		switch (opcion) {
		case 1:
			System.out.println("Introduce una cadena");
			String cadena = input.nextLine();
			if (cadena.charAt(0) == cadena.charAt(cadena.charAt(4-1))) {
				System.out.println("Si se cumple");
			} else {System.out.println("No se cumple");}			
			break;
		case 2:
			System.out.println("Introduce un numero de 3 cifras");
			int entero = input.nextInt();
			if (entero >= 0) {
				System.out.println(entero /100);
				System.out.println((entero /10) %10 );
				System.out.println((entero %10));
			} else {System.out.println();}
			break;
		case 3:
			System.out.println("Has salido del programa");
			break;
		default:
			System.out.println("Opcion incorrecta");			
		}	
		input.close();
		
	}

}

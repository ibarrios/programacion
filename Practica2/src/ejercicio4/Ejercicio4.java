package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		String cadena = "";
		int sumaEnteros = 0;
		int numeroMayor = 0;
		int numero = 0;

		do {
			System.out.println("Introduce numeros enteros");
			cadena = input.nextLine();
			if (cadena.contentEquals("fin") == false ) {

				 numero  = Integer.parseInt(cadena); 
				sumaEnteros = sumaEnteros + numero;
				
			}
			numeroMayor = Math.max(numeroMayor, numero);



		} while (cadena.contentEquals("fin") != true);

		System.out.println("La suma de los enteros es " + sumaEnteros);
		System.out.println("El numero mayor de todos es " + numeroMayor);
		
		
		input.close();
	}

}

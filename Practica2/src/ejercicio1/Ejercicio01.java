package ejercicio1;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		int opcion;

		do {
			System.out.println("Introduce una opcion \n"
					+ "1- Contar mayusculas\n"
					+ "2- Contrasenia Correcta\n"
					+ "3- Salir");
			opcion = input.nextInt();
			input.nextLine();		 
			switch (opcion) {
			case 1:
				System.out.println("Introduce una cadena");
				String cadena = input.nextLine();
				int cantidadMayusculas = 0;
				for ( int i = 0 ;  i < cadena.length(); i++ ) {
					if (cadena.charAt(i) >= 'A' && cadena.charAt(i) <= 'Z') {
						cantidadMayusculas++;
					}				 
				}
				System.out.println("La cantidad de mayusculas en la cadena es  " + cantidadMayusculas);
				break;
			case 2:
				System.out.println("Introduce contraseņa");
				String clave = input.nextLine().toLowerCase();			
				int passCorrecta1 = 0;
				int passCorrecta2 = 0;
				for ( int i = 0 ;  i < clave.length() ; i++ ) {
					if (clave.charAt(i) == 'a' || clave.charAt(i) == 'e' || clave.charAt(i) == 'i' || clave.charAt(i) == 'o' || clave.charAt(i) == 'u' ) {
						passCorrecta1++;
					} 
				}
				for ( int i = 0 ;  i < clave.length(); i++ ) { 					
					if (clave.charAt(i) >= 'a' && clave.charAt(i) <= 'z') {							

					} else {passCorrecta2++;}
				}
				if (passCorrecta1 > 0 && passCorrecta2 == 0) {
					System.out.println("La clave es correcta");
				} else {
					System.out.println("La clave es incorrecta");
				}
				break;
			case 3:
				System.out.println("Has salido del programa");
				break;
			default:
				System.out.println("Opcion incorrecta");
				break;
			}
		} while (opcion != 3);
		input.close();
	}

}

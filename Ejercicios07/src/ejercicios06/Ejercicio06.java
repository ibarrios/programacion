package ejercicios06;

import java.util.Scanner;

public class Ejercicio06 {
	/*
	 * Programa que pida por teclado n�meros y rellene dos arrays de enteros de 10 elementos
(20 n�meros). Posteriormente llamar� a un m�todo que recibe ambos arrays y que crear�
y devolver� un array de 10 elementos organizados de la siguiente forma: El elemento 0 es
la suma de los elementos 0 de ambos arrays, el elemento 1 es la suma de los elementos 1
de ambos arrays, etc. Para obtener el array con la suma de los otros 2 debo usar un bucle.
	 * 
	 */
	
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int primerArray[] = new int [10];
		int segundoArray[] = new int [10];
		int tercerArray[] = new int [primerArray.length];
		for (int i = 0; i < primerArray.length; i++) {
			primerArray[i] = input.nextInt();
		}
		
		for (int i = 0; i < segundoArray.length; i++) {
			segundoArray[i] = input.nextInt();
		}
		
		for (int i = 0; i < tercerArray.length; i++) {
			tercerArray[i] = primerArray[i]+segundoArray[i];
		}
		
		for (int i = 0; i < tercerArray.length; i++) {
			System.out.println("La componente " + i + " es " + tercerArray[i]);
		}
		input.close();
	}
}

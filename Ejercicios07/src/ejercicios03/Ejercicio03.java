package ejercicios03;

public class Ejercicio03 {
	/*
	 * Crear un programa que muestra un array de caracteres. Desde el m�todo main se
construye y rellena un array con las letras desde la �a� a la �k�. No aleatoriamente, sino que
la posici�n 0 del array tendr� la �a�, la posici�n 1 tendr� la �b�, y as� sucesivamente hasta la
�k�. Posteriormente se llamar� a un m�todo que recibe el array como par�metro, modifica
cada vocal por el car�cter �*� y lo devuelve al programa principal. Despu�s el programa
principal mostrar� el contenido del array que ha devuelto el m�todo.

	 * 
	 */
	
	public static char[] cambiarArray(char []array) {
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] == 'a' || array[i] == 'e' || array[i] == 'i' || array[i] == 'o' || array[i] == 'u')
			array[i] = '*';
		}
		return array;
	}
	
	
	public static void main(String[] args) {
		
		char array[] = {'a','b','c','d','e','f','g','h','i','j','k'};

		cambiarArray(array);
	
			for (int i = 0; i < array.length; i++) {
				System.out.println("La componenete " + i + " es " + array[i]);
			}	
	}
}

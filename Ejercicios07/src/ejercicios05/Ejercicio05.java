package ejercicios05;

import java.util.Scanner;

public class Ejercicio05 {
	/*
	 * Crear un programa que pida 10 n�meros enteros y los guarde en un array de 10
elementos. Posteriormente se llamar� a un m�todo que recibe dicho array, y nos mostrar�
	 por pantalla una serie de estad�sticas: cu�l es el mayor, y cu�l es el menor, la media de los
positivos y de los negativos, y la media de todos.* 
	 */


	public static void estadisticasArray(int [] array) {
		
		int enteroMayor = 0;
		int enteroMenor = 0 ;
		int contadorPositivos = 0;
		int contadorNegativos = 0;
		int sumaPositivos = 0;
		int sumaNegativos = 0;
		int sumaTotal = 0;
		
		for (int i = 0; i < array.length; i++) {
			
			 if (array[i]>enteroMayor) {
				enteroMayor = array[i];
			}  
			 if (array[i]<enteroMenor) {
				 enteroMenor = array[i];
			 }
			
			 if (array[i] > 0 ) {
				 contadorPositivos++;
				 sumaTotal += array[i];
				 sumaPositivos += array[i];
			 }
			 if (array[i]<0 ) {
				 contadorNegativos++;
				 sumaTotal += array[i];
				 sumaNegativos += array[i];
				 
			 }		
		}
		
		System.out.println("El mayor del array es " + enteroMayor); 
		System.out.println("El menor del array es " + enteroMenor);
		System.out.println("La mendia de los positivos es  " + sumaPositivos/contadorPositivos);
		System.out.println("La media de los negativos es " + sumaNegativos/contadorNegativos);
		System.out.println("La media total es " + sumaTotal/array.length);

	}


	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce los 10 numeros del array");
		int array[] = new int [10];
		for (int i = 0; i < array.length; i++) {
			array[i]=input.nextInt();
		}
		
		estadisticasArray(array);
		
		
		input.close();
	}
}

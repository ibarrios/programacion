package ejercicios11;

import java.util.Scanner;

public class Ejercicio11 {
	/*
	 * Crear un programa que pida n�meros para rellenar una matriz de 3 x 3. Despu�s llamar� a
un m�todo que recibe dicho array y me muestra por pantalla una serie de estad�sticas: el
n�mero mayor, el n�mero menor, la media de todos y el producto de todos.

	 * 
	 *
	 */
	
	
	public static void estadisticasMatriz(int matriz[][]) {
		
		int enteroMayor = 0;
		
		int sumaEntero = 0;
		int productoEntero = 1;
		int enteroMenor = matriz[0][0];
		
		
		for (int i = 0; i < matriz.length; i++) {
			
			for (int j = 0; j < matriz[i].length; j++) {
				
				 if (matriz[i][j]>enteroMayor) {
						enteroMayor = matriz[i][j];
					} else if (matriz[i][j] < enteroMenor) {
					 enteroMenor = matriz[i][j];
				 }		 
				sumaEntero += matriz[i][j];	 
				productoEntero = matriz[i][j];
				
			}
			
		}
		
		System.out.println("El mayor de la matriz es "  + enteroMayor);
		System.out.println("El nenor de la matriz es "  + enteroMenor);
		System.out.println("Suma de la matriz es  "  + sumaEntero);
		System.out.println("El producto de la matriz es  "  + productoEntero);
		
	}
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int matriz[][] = new int [3][3];
		
		for(int i = 0; i < matriz.length; i++){
						
			   for(int j = 0; j< matriz[i].length; j++){
			      System.out.println("Introduce un entero");
					matriz[i][j] = input.nextInt();	
			   }
			}
		
		estadisticasMatriz(matriz);
		
		
		input.close();
	}
}



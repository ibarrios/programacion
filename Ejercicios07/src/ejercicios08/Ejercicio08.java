package ejercicios08;

import java.util.Scanner;

public class Ejercicio08 {
	/*
	 * Programa que pida por teclado n�meros y rellene dos arrays de enteros de 10 elementos
(20 n�meros). Posteriormente llamar� a un m�todo que devolver� un array de 10
elementos organizados de la siguiente forma: El elemento 0 es la suma de los elementos 0
y 9 de ambos arrays, el elemento 1 es la suma de los elementos 1 y 8 de ambos arrays, etc.
Realizar la suma recorriendo el array resultante con un bucle.
	 * 
	 */
	
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int primerArray[] = new int [10];
		int segundoArray[] = new int [10];
		int tercerArray[] = new int [primerArray.length];
		int contador= tercerArray.length-1;
		System.out.println("Introduce los 10 numeros del primer array");
		for (int i = 0; i < primerArray.length; i++) {
			primerArray[i] = input.nextInt();
		}
		System.out.println("Introduce los 10 numeros del segundo array");	
		for (int i = 0; i < segundoArray.length; i++) {
			segundoArray[i] = input.nextInt();
		}
		
		for (int i = 0; i < tercerArray.length; i++) {
			tercerArray[i] = (primerArray[i]+primerArray[contador])+(segundoArray[i]+segundoArray[contador]);
			contador--;
			
		}
		for (int i = 0; i < tercerArray.length; i++) {
			System.out.println("La componente " + i + " es " + tercerArray[i]);
		}
		
		
		input.close();
	}
}

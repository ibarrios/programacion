package ejercicios09;

import java.util.Scanner;

public class Ejercicio09 {
	/*
	 * Programa que rellene un array de 10 elementos con n�meros ordenados. (No es necesario
pedirlos al usuario, se pueden indicar desde el c�digo: 10 n�meros por ejemplo de 10 en
10). Posteriormente el programa pedir� un n�mero al usuario, y este debe insertarse en la
posici�n del array exacta, para que el array siga ordenado. Finalmente se muestra el
contenido del array
	 * 
	 */
	public static void main(String[] args) {


		Scanner input =  new Scanner(System.in);
		int array[] = {0,10,20,30,40,50,60,70,80,90};
		System.out.println("Introduce un entero");
		int entero = input.nextInt();
		int contador = 1;
		for (int i = 0; i < array.length; i++) {


			if (entero < array[i]) {
				array[i] = entero ;
				break;
			} else if(entero > array[array.length-1]) {
				array[array.length-1] = entero ;
				break;				
			} else if (array[i] < entero && array[contador] > entero) {
				contador++;
				array[i]=entero;
				break;
			}


		}

		for (int i = 0; i < array.length; i++) {
			System.out.println("la componente " + i + " es " + array[i]);
		}

		input.close();
	} 
}

package ejercicios01;

import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio01 {
	
	/*
	 * Crear una aplicaci�n que pida 10 n�meros enteros, y posteriormente muestre esos
n�meros en una l�nea, mostrando desde el �ltimo al primero, en ese orden. Se crear� un
m�todo que construye un array, pide los n�meros y los guarda en �l. Posteriormente dicho
m�todo devolver� el array, y ser� el programa principal quien los muestre en orden
inverso (1� la �ltima celda del array, y as� sucesivamente).
	 * 
	 */
	
	public static int [] arrayLeido() {
		int array[] = new int [10];
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce las componentes del array");
		for (int i = 0; i < array.length; i++) {
			 array[i] = input.nextInt();
		}
		
		input.close();
		return array;
	}
	
	public static void main(String[] args) {
		int array[] = arrayLeido();
		
		Arrays.sort(array);
		int j = array.length-1;
		for (int i = 0; i < array.length; i++) {
			
			System.out.print( array[j] + " ");
			j--;
		}


	}
}

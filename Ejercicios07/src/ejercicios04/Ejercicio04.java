package ejercicios04;

import java.util.Scanner;

public class Ejercicio04 {
	/*
	 * Crear un programa parecido al anterior. El programa desde el m�todo main debe pedirnos
por teclado el n�mero de elementos del array, y posteriormente construye un array y lo
rellene con letras aleatorias de la �a� a la �z�. Mostrar� el array por pantalla. Despu�s,
debemos llamar al m�todo del ejercicio anterior (debe ser p�blico) que modificar� nuestro
array y nos lo devolver�. Posteriormente lo mostraremos por pantalla.
	 * 
	 */
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce longitud del array");
		int longArray = input.nextInt();
		char array[] = new char [longArray];
		
		
		for (int i = 0; i < array.length; i++) {
			array[i] = (char)('a'+(Math.random()*25));
		}
		
		for (int i = 0; i < array.length; i++) {
			System.out.println("La componente "+ i + " es " + array[i]);
		}
		
		ejercicios03.Ejercicio03.cambiarArray(array);
		
		for (int i = 0; i < array.length; i++) {
			System.out.println("La componente "+ i + " es " + array[i]);
		}
		input.close();
	}
	
	
}

package ejercicios07;

import java.util.Scanner;

public class Ejercicio07 {
	/*
	 *Programa que pide un tama�o de array al usuario y posteriormente rellena ese array con
Strings pidi�ndolas al usuario. Despu�s llama al m�todo desplazar(String[] array, int
desplazamiento) y mueve cada string tantas posiciones adelante como indica
desplazamiento. Por ejemplo, si desplazamiento es 3, el string de la posici�n 0 pasar� a la
posici�n 3, y as� sucesivamente. Cuando nos pasamos de la longitud del array, debemos
colocarlos al principio. Despu�s el programa principal lo debe mostrar por pantalla. 
	 * 
	 */
	
	public static void desplazar(String[] array,int desplazamiento) {
		
		
		for (int i = 0; i < array.length; i++) {
			array[i+desplazamiento] = array[i];
		}

	}
	
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);	
		System.out.println("Introduce la longitud del array");
		int longArray = input.nextInt();
		String array[] = new String [longArray];
	
		for (int i = 0; i < array.length; i++) {
			System.out.println("Introduce cadenas");
			array[i]=input.nextLine();
		}
		
		System.out.println("Introduce el desplazamiento");
		
		
		
		input.close();
	}

}


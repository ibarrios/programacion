package ejercicios10;


import java.util.Scanner;

public class Ejercicio10 {
	/*
	 * Programa que pida caracteres (o n�meros) por teclado y rellene un array de 10 elementos.
Posteriormente mostrar� dicho array. Acto seguido llamar� a un m�todo que ordenar� el
array de menor a mayor. Finalmente, el programa principal muestra de nuevo el array.
	 * 
	 */

	public static void ordenarArray(int [] array) {
		int variableTemporal = 0;
	
		for (int i = 0; i < array.length; i++) {
			for (int j = 1; j < (array.length - i); j++) {
				if (array[j - 1] > array[j]) {
					variableTemporal = array[j - 1];
					array[j - 1] = array[j];
					array[j] = variableTemporal;
				}
			}
		}
	}
	

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Introduce numeros por teclado");
		int array[] = new int [10];

		for (int i = 0; i < array.length; i++) {
			array[i] = input.nextInt();
		}

		ordenarArray(array);
		for (int i = 0; i < array.length; i++) {
			System.out.println("La componente " + i + " es " + array[i]);
		}

		input.close();
	}
}

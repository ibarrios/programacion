package ejercicios14;


import java.util.Scanner;

public class Ejercicio14 {
	/*
	 * Crear una aplicación que me pida el nombre de un alumno y su edad (5 alumnos). Se
deben almacenar en una matriz de String de 2x5. Posteriormente me mostrará los datos
de cada alumno y su edad.
	 * 
	 */
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		String matriz[][] = new String [5][2];
		
		System.out.println("Introduce alumno y edad");
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				
				matriz[i][j] = input.next();
				
			}
		}
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println("");
		}
		
		
		
		
		input.close();
	}
}

package ejercicios13;

import java.util.Scanner;

public class Ejercicio13 {
	/*
	 * rograma que rellene una matriz de 10 x 10 con n�meros aleatorios entre 0 y 49, ambos
incluidos. Tambi�n me pedir� un n�mero para buscarlo dentro de la matriz
Posteriormente llamar a un m�todo que recibe dicha matriz, y el n�mero pedido. Este
m�todo debe buscar en la matriz el n�mero que yo he introducido, y me debe mostrar por
pantalla el lugar donde se encuentra cada vez que aparezca [fila, columna]. Finalmente me
debe decir el n�mero de veces que lo ha encontrado. Si no lo ha encontrado ninguna vez,
l�gicamente no me mostrar� ninguna coordenada.
	 * 
	 */


	
	public static void buscarMatriz(int matriz [][], int entero) {
		int contador = 0;
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (matriz[i][j] == entero ) {
					System.out.println("El numero " + entero + " esta en la fila " + i + " columna " + j);
					contador++;
				}
				
			}
		}
		
		System.out.println("el numero " + entero + " aparece " + contador + " veces");
		
		
	}
	
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int matriz[][] = new int [10][10];

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j] = (int)(Math.random()*49);
	
			}
		}
		System.out.println("Introduce el entero a buscar");
		int enteroBuscar = input.nextInt();
		
		
		buscarMatriz(matriz, enteroBuscar);
		input.close();
	}	
}

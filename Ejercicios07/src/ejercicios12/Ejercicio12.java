package ejercicios12;

import java.util.Scanner;

public class Ejercicio12 {
	/*
	 * 
	 * rear un programa que pida un n�mero para crear una matriz cuadrada (3 ->3x3, 4 ->4x4,
5 ->5x5, etc). Despu�s llamar� a un m�todo que recibe la matriz y el objeto Scanner y que
nos pedir� n�meros para rellenar dicha matriz . Dicho m�todo la muestra tambi�n.
Posteriormente se llamar� a un m�todo que recibe la matriz y me crea otra que es el
resultado de intercambiar las filas por las columnas. Posteriormente el programa principal
recibe esta matriz modificada y la muestra por pantalla.
	1 2 3 	 1 4 7
	4 5 6 -> 2 5 8
	7 8 9	 3 6 9
	 */
	public static Scanner input = new Scanner(System.in);

	public static void llenarMatriz(int [][] matriz) {

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j]=input.nextInt();

			}
		}

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j]);
			}
			System.out.println("");
		}

	}
	
	public static void modificarMatriz(int[][] matriz ) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j]=matriz[i][i];
				
				
			}
		}
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j]);
			}
			System.out.println("");
		}
		
		
	}
	

	public static void main(String[] args) {

		System.out.println("introduce el lado de la matriz");
		int longMatriz = input.nextInt();
		int matriz[][] = new int [longMatriz][longMatriz];

		llenarMatriz(matriz);
		
		
		modificarMatriz(matriz);
	}


}
